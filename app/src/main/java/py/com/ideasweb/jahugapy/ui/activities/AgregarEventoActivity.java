package py.com.ideasweb.jahugapy.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import py.com.ideasweb.jahugapy.R;

import py.com.ideasweb.jahugapy.api.manager.ThreadManagerEvento;
import py.com.ideasweb.jahugapy.config.Config;
import py.com.ideasweb.jahugapy.pojo.Cancha;

import py.com.ideasweb.jahugapy.pojo.Evento;
import py.com.ideasweb.jahugapy.pojo.User;
import py.com.ideasweb.jahugapy.pojo.Userapp;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.Share;
import py.com.ideasweb.jahugapy.utilities.Utilities;
import py.com.ideasweb.jahugapy.utilities.Validation;



public class AgregarEventoActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener{
    private Toolbar mToolbar;
    TextView mTitle;
    TextView hora;
    TextView fecha;
    TextView nombre;
    TextView cantidad;
    TextView comentarios;

    SearchableSpinner cancha;
    MaterialBetterSpinner deporte;
    MaterialBetterSpinner dimension;
    MaterialBetterSpinner superficie;
    Cancha canchaSeleccionada;


    String[] DEPORTES = {"FUTBOL", "PADEL", "VOLLEY", "STREET SOCCER", "PIKI VOLLEY"};
    String[] DIMENSION = {"5 vs 5", "6 vs 6", "7 vs 7", "11 vs 11", "2 vs 2"};
    String[] SUPERFICIE = {"SINTETICO", "PISO", "NATURAL", "PARQUET"};


    //dimemsion
    CheckBox invitar;
    Button btnagregar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_evento);

        // se agrega el toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        //se centra el titulo
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        mTitle.setText(mToolbar.getTitle());
        //se oculta el title por defecto
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hora = (EditText) findViewById(R.id.tvEventoHora);
        fecha = (EditText) findViewById(R.id.tvEventoFecha);
        nombre = (EditText) findViewById(R.id.AddNombreEvento);
        cantidad = (EditText) findViewById(R.id.addCantidadEvento);

        //canchas
        ArrayAdapter<Cancha> ciudadList = new ArrayAdapter<Cancha>(this,android.R.layout.simple_dropdown_item_1line, ReciclerViewPresenter.canchasCercanas);


        cancha = (SearchableSpinner)findViewById(R.id.addCanchaEvento);
        cancha.setAdapter(ciudadList);


        /*cancha.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                canchaSeleccionada = (Cancha) adapterView.getItemAtPosition(i);


            }
        });*/



        //depportes
        ArrayAdapter<String> arraydeportes = new ArrayAdapter<String>(this,   android.R.layout.simple_dropdown_item_1line, DEPORTES);
        deporte = (MaterialBetterSpinner)findViewById(R.id.addEventoDeporte);
        deporte.setAdapter(arraydeportes);
        //dimension
        ArrayAdapter<String> arraydimension = new ArrayAdapter<String>(this,   android.R.layout.simple_dropdown_item_1line, DIMENSION);
        dimension = (MaterialBetterSpinner)findViewById(R.id.addEventoDimension);
        dimension.setAdapter(arraydimension);
        //superficie
        ArrayAdapter<String> arraysuperficie = new ArrayAdapter<String>(this,   android.R.layout.simple_dropdown_item_1line, SUPERFICIE);
        superficie = (MaterialBetterSpinner)findViewById(R.id.addEventoSuperficie);
        superficie.setAdapter(arraysuperficie);

        invitar = (CheckBox) findViewById(R.id.checkInvitarEvento);
        comentarios = (TextView) findViewById(R.id.addComentariosEvento);

        btnagregar = (Button) findViewById(R.id.addEventoAceptar);



    }

    //infla en el activity el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // menu de opciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mabout:
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                        .customView(R.layout.layout_acercade, wrapInScrollView)
                        .positiveText("OK")
                        .show();
                break;
            case R.id.mshare:
                Share.shareRedeSociales(this, Config.API_DOWNLOAD_APK, "JahugaPy - Encuentra las canchas deportivas cercanas a ti!!");
                break;
            case R.id.mcontact:
                Intent intent = new Intent(this, InfoActivity.class);
                intent.putExtra("fragment", "contacto");
                startActivity(intent);
                finish();
                break;
            case android.R.id.home:
                Intent i = new Intent(AgregarEventoActivity.this, EventosActivity.class);
                startActivity(i);
                finish();

                break;
            case R.id.mfanPage:
                irFacebook();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){

        if(keyCode == KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(AgregarEventoActivity.this, EventosActivity.class);
            startActivity(intent);
            finish();

        }
        return super.onKeyDown(keyCode, event);
    }

    public void verPicker(View view){
        //time
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                AgregarEventoActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.show(getFragmentManager(), "Hora de Atencion");
    }

    public void verPickerFecha(View view){
        //date
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                AgregarEventoActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        dpd.setAutoHighlight(true);
        dpd.show(getFragmentManager(), "Dia del Evento");
    }

    public Object[] separarHora(String hora){
        Object [] obj = new Object[2];
        String[] parts = hora.split("a");
        obj[0] = parts[0].trim();
        obj[1] = parts[1].trim();

        return obj;
    }

    /*public void iniciarlizarCanchas(){

        for (Cancha element: ReciclerViewPresenter.canchasCercanas) {
            listcanchas.add(new Cancha(element.getIdcancha(), element.getDenominacion()));
        }
    }*/

    public void agregarEvent(View view) throws ParseException {
        Evento evento = new Evento();
        if(Utilities.isNetworkConnected(getApplicationContext())){
            if(checkValidation()){
                System.out.println("Insertando evento");
                evento.setDescripcion(nombre.getText().toString().toUpperCase());
                evento.setComentarios(comentarios.getText().toString());

                if(cantidad.getText().toString() != ""){
                    evento.setTotalinvitados(Integer.parseInt(cantidad.getText().toString()));
                }else{
                    evento.setTotalinvitados(0);
                }
                canchaSeleccionada = (Cancha) cancha.getSelectedItem();
                evento.setCancha(canchaSeleccionada);

                Object [] obj = new Object[2];
                obj = separarHora(hora.getText().toString());

                SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyy HH:mm");
                Date parsedDate = format.parse(fecha.getText().toString() + " " + obj[0].toString());
                Date parsedDate1 = format.parse(fecha.getText().toString() + " " + obj[1].toString());

                Timestamp  hd = new java.sql.Timestamp(parsedDate.getTime());
                Timestamp   ha =  new java.sql.Timestamp(parsedDate1.getTime());

                System.out.println(fecha.getText().toString() + " " + obj[0].toString());

                evento.setHoradesde(hd);
                evento.setHorahasta(ha);

                System.out.println(User.getIduser());
                evento.setEstado(invitar.isChecked());
                Userapp u = new Userapp();
                u.setIduser(User.getIduser());
                evento.setUserapp(u);


                if(deporte.getText().toString().equals("FUTBOL")){
                    evento.setDeporte(1);
                }else if(deporte.getText().toString().equals("PADEL")){
                    evento.setDeporte(2);
                }else if(deporte.getText().toString().equals("VOLLEY")){
                    evento.setDeporte(3);
                }else if(deporte.getText().toString().equals("STREET SOCCER")){
                    evento.setDeporte(4);
                }else{
                    evento.setDeporte(5);
                }

                if(superficie.getText().toString().equals("SINTETICO")){
                    evento.setSuperficie(1);
                }else if(superficie.getText().toString().equals("PISO")){
                    evento.setSuperficie(2);
                }else if(superficie.getText().toString().equals("NATURAL")){
                    evento.setSuperficie(3);
                }else if(superficie.getText().toString().equals("PARQUET")) {
                    evento.setSuperficie(4);
                }

                if(dimension.getText().toString().equals("5 vs 5")){
                    evento.setDimension(1);
                }else if(dimension.getText().toString().equals("6 vs 6")){
                    evento.setDimension(2);
                }else if(dimension.getText().toString().equals("7 vs 7")){
                    evento.setDimension(3);
                }else if(dimension.getText().toString().equals("11 vs 11")) {
                    evento.setDimension(4);
                }else{
                    evento.setDimension(5);
                }


                ThreadManagerEvento threadManager =  new ThreadManagerEvento( "insertEvento",evento, "Evento Creado!", getCurrentFocus());
                threadManager.execute();
            }
        }else{
            Snackbar.make(getCurrentFocus(), "Verifique su conexion a Internet", Snackbar.LENGTH_LONG).show();
        }


    }


    // validacion de formulario
    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText((EditText) findViewById(R.id.AddNombreEvento))) ret = false;
        if (!Validation.hasText((EditText) findViewById(R.id.tvEventoFecha))) ret = false;
        if (!Validation.hasText((EditText) findViewById(R.id.tvEventoHora))) ret = false;
        if(invitar.isChecked()){
            if (!Validation.hasText((EditText) findViewById(R.id.addCantidadEvento))) ret = false;
        }

        if(dimension.getText().toString().length() == 0){
            ret = false;
            dimension.setError("Campo Requerido");
        }
        if(superficie.getText().toString().length() == 0){
            ret = false;
            superficie.setError("Campo Requerido");
        }
        if(deporte.getText().toString().length() == 0){
            ret = false;
            deporte.setError("Campo Requerido");
        }
        if(cancha.getSelectedItemPosition() == -1){
            ret = false;
            Toast.makeText(getApplicationContext(), "Seleccione el lugar", Toast.LENGTH_LONG).show();
        }
        System.out.println("item: " + cancha.getSelectedItemPosition());

        return ret;
    }

    public void irFacebook( ) {

        try {
            getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/169405010203582"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent=  new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/jahugapy"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkJaime(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://jaimeferreira91"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/jaimeferreira91"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkMarcos(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://marcos-cespedes-8aa92319"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/marcos-cespedes-8aa92319"));
            startActivity(intent);
        }
    }

    public void irLinkEmilio(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://juan-emilio-espinola-874716125"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/juan-emilio-espinola-874716125"));
            startActivity(intent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");
        if(dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        String minuteString = minute < 10 ? "0"+minute : ""+minute;
        String hourStringEnd = hourOfDayEnd < 10 ? "0"+hourOfDayEnd : ""+hourOfDayEnd;
        String minuteStringEnd = minuteEnd < 10 ? "0"+minuteEnd : ""+minuteEnd;
        String time = hourString+":"+minuteString+" a "+hourStringEnd+":"+minuteStringEnd;


        hora.setText(time);

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

        //String date = dayOfMonth+"/"+(++monthOfYear)+"/"+year+" a "+dayOfMonthEnd+"/"+(++monthOfYearEnd)+"/"+yearEnd;
        String date = dayOfMonth+"/"+(++monthOfYear)+"/"+year;
        fecha.setText(date);
    }



}
