package py.com.ideasweb.jahugapy.api;

import android.content.Context;

import java.util.ArrayList;

import py.com.ideasweb.jahugapy.pojo.Ciudad;
import py.com.ideasweb.jahugapy.pojo.CiudadRepo;

/**
 * Created by jaime on 10/12/16.
 */

public class ConstructorCiudades {

    private Context context;

    public ConstructorCiudades(Context context) {
        this.context = context;
    }

    public ArrayList<Ciudad> ObtenerCiudades() throws Exception{



        return CiudadRepo.cargarCiudades(context);

       // return manager.getCanchas();
    }
}
