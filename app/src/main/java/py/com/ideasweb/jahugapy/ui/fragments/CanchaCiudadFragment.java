package py.com.ideasweb.jahugapy.ui.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import py.com.ideasweb.jahugapy.Adapter.CanchaAdaptador;
import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.pojo.Respuesta;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * A simple {@link Fragment} subclass.
 */
public class CanchaCiudadFragment extends Fragment implements IReciclerView{

    private RecyclerView canchasCercanas;
    private String ciudad;
    private String idciudad;
    private SwipeRefreshLayout refreshLayout;

    public CanchaCiudadFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_cancha_ciudad, container, false);
        canchasCercanas = (RecyclerView) view.findViewById(R.id.rvCiudadCancha);

        Bundle b = getArguments();
        if(b != null){
            idciudad = b.getString("idciudad");
            ciudad = b.getString("ciudad");

            if(Utilities.isNetworkConnected(getContext()) && MiUbicacion.getLatitud() != null) {
               new ReciclerViewPresenter(CanchaCiudadFragment.this, getContext(), "ciudades", idciudad, false);
            }else{
                Toast.makeText(getContext(), "Verfifique su conexion y recarge", Toast.LENGTH_LONG).show();
            }

        }

        TextView tvfiltro = (TextView) view.findViewById(R.id.tvFiltroCiudad);
        tvfiltro.setText(ciudad);

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.indicadorRefreshCC);
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new ReciclerViewPresenter(CanchaCiudadFragment.this, getContext(), "ciudades", idciudad, true);
                refreshLayout.setRefreshing(false);
            }
        });



        return view;
    }

    @Override
    public void generarLineaLayoutVertical() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        canchasCercanas.setLayoutManager(llm);
    }

    @Override
    public CanchaAdaptador crearAdaptador(List<Cancha> canchas) {
        CanchaAdaptador adaptador = new CanchaAdaptador(canchas, getActivity());
        return adaptador;
    }



    @Override
    public void inicializarAdaptadorRV(CanchaAdaptador adapatador) {
        canchasCercanas.setAdapter(adapatador);
    }
}
