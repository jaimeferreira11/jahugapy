package py.com.ideasweb.jahugapy.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import py.com.ideasweb.jahugapy.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Root3Fragment extends Fragment {


    public Root3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_root3, container, false);

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        transaction.replace(R.id.root_frame3, new AllMapsFragment());

        transaction.commit();

        return view;
    }

}
