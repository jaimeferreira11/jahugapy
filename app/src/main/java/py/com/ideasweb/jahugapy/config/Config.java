package py.com.ideasweb.jahugapy.config;

/**
 * Created by jaime on 23/11/16.
 */

public class Config {

    public static final String EMAIL ="ideaswebpy@gmail.com";
    public static final String PASSWORD ="ideasweb2017";

    public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    public static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    /**
     * URL connections properties conf.
     * amazon ws
     */
    /*public static final String urlBase = "http://www.fundacionparaguaya.org.py";
    public static final String port    = "";
    public static final String context = "/hercules-rest";*/

    //ideasweb hosting
    public static final String urlBase = "http://www.ideasweb.com.py";
    public static final String port    = "";
    public static final String context = "/hercules-rest";

    //localhost
   /* public static final String urlBase = "http://10.0.10.11:";
    public static final String port    = "8080";
    public static final String context = "/hercules-rest";*/
    // prefijo para publico y privado

    private static final String private_url = urlBase+port+context+"/private";
    private static final String public_url = urlBase+port+context+"/public";

    /**
     * metodos para llamar a las APIs publicas
     */
    public static final String API_GET_CANCHAS_BY_LOCALIZATION = public_url+"/canchas/";
    public static final String API_GET_CIUDADES = public_url+"/distrito/all";
    public static final String API_GET_CANCHAS_BY_CIUDAD = public_url+"/canchas/";
    public static final String API_GET_CANCHAS_BY_PARAM = public_url+"/canchaspordenominacion/";
    public static final String API_GET_CANCHAS_BY_FILTRO = public_url+"/canchas/canchasporfiltro";
    public static final String API_POS_INSERT_CANCHA = public_url+"/cancha-register";
    public static final String API_DOWNLOAD_APK ="https://play.google.com/store/apps/details?id=py.com.ideasweb.jahugapy&hl=es_419";
    public static final String API_POST_USER_REGISTER = public_url+"/user-register/";
    public static final String API_GET_EVENTOS = public_url+"/eventos";
    public static final String API_POST_EVENTO = public_url+"/evento";
    public static final String API_POST_ASISTIR_EVENTO = public_url+"/evento/asistir/";

}
