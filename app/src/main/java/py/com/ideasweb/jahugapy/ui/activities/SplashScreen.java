package py.com.ideasweb.jahugapy.ui.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.appcompat.BuildConfig;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.net.SocketTimeoutException;
import java.util.Timer;
import java.util.TimerTask;

import py.com.ideasweb.jahugapy.NotificationService;
import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.api.ConstructorCanchas;
import py.com.ideasweb.jahugapy.api.manager.ThreadManager;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.pojo.User;
import py.com.ideasweb.jahugapy.ui.fragments.CercanosFragment;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.CircleTransform;
import py.com.ideasweb.jahugapy.utilities.GPSTracker;
import py.com.ideasweb.jahugapy.utilities.Utilities;


public class SplashScreen extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener{

    // Set the duration of the splash screen
    private static final long SPLASH_SCREEN_DELAY = 2000;
    String versionName = BuildConfig.VERSION_NAME;
    private GPSTracker gps;

    //login whit facebook
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //login facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();
        // Set portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);
        TextView tversion = (TextView) findViewById(R.id.tvVersion);
        tversion.setText(versionName);
        gps = new GPSTracker(this);
        gps.getLocation();

        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        tversion.setText(pinfo.versionName);

        getAccount();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // Start the next activity
                Intent mainIntent = new Intent().setClass(SplashScreen.this, Main2Activity.class);
                mainIntent.putExtra("origen", "cercanos");
                startActivity(mainIntent);
                finish();
            }
        };

        // Simulate a long loading process on application startup.
   //     if (Utilities.isNetworkConnected(this) && MiUbicacion.getLatitud() != null) {
            getCanchasCercanas();
            Timer timer = new Timer();
            timer.schedule(task, SPLASH_SCREEN_DELAY);

       /* } else {
            Timer timer = new Timer();
            timer.schedule(task, SPLASH_SCREEN_DELAY);
        }*/


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // Name, email address, and profile photo Url
                    User.setName(user.getDisplayName());
                    User.setEmail( user.getEmail());
                    User.setGoogleid(user.getUid());
                    User.setPhoto(user.getPhotoUrl().toString());
                    User.setIddevice(FirebaseInstanceId.getInstance().getToken());
                    User.setLogueado(true);


                    String name = user.getDisplayName();
                    String email = user.getEmail();
                    Uri photoUrl = user.getPhotoUrl();



                    System.out.println(name + " - " + email );
                    System.out.println(photoUrl);

                    // The user's ID, unique to the Firebase project. Do NOT use this value to
                    // authenticate with your backend server, if you have one. Use
                    // FirebaseUser.getToken() instead.
                    String uid = user.getUid();
                } else {
                    User.setLogueado(false);
                    Log.d("face", "onAuthStateChanged:signed_out");

                }


                // ...
            }
        };



    }

    private void getAccount() {

        String email = "";
        try {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");
            for (Account account : accounts) {

                if(account.name.toUpperCase().contains("GMAIL")){
                    email = account.name;
                    break;
                }

            }
            //whatsapp
            User.setEmail(email);
            User.setIddevice(FirebaseInstanceId.getInstance().getToken());

        }
        catch(Exception e)
        {
            Log.i("Exception", "Exception:"+e) ;
        }

        Log.i("Exception", "mails:"+User.getEmail() + " id: "+ User.getIddevice()) ;
    }


    public void getCanchasCercanas() {
        try{
            /*ThreadManager threadManager =  new ThreadManager(this, "getCanchas", new Cancha(), "", getCurrentFocus());
            threadManager.execute();*/

            if(User.getLogueado()){
                ThreadManager threadManager2 =  new ThreadManager(this, "registerUser", new Cancha(), "", getCurrentFocus());
                threadManager2.execute();
            }

        }catch (Exception e){
            Intent mainIntent = new Intent().setClass(SplashScreen.this, Main2Activity.class);
            mainIntent.putExtra("origen", "cercanos");
            startActivity(mainIntent);

            finish();

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("GoogleSignIn", "OnConnectionFailed: " + connectionResult.toString());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}
