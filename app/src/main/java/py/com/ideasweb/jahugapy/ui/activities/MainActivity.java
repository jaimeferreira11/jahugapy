package py.com.ideasweb.jahugapy.ui.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.appcompat.BuildConfig;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import py.com.ideasweb.jahugapy.Adapter.PageAdapter;
import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.api.manager.ThreadManager;
import py.com.ideasweb.jahugapy.config.Config;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.ui.fragments.AllMapsFragment;
import py.com.ideasweb.jahugapy.ui.fragments.CercanosFragment;
import py.com.ideasweb.jahugapy.ui.fragments.CiudadesFragment;
import py.com.ideasweb.jahugapy.ui.fragments.PersonalizadoFragment;
import py.com.ideasweb.jahugapy.ui.fragments.Root2Fragment;
import py.com.ideasweb.jahugapy.ui.fragments.Root3Fragment;
import py.com.ideasweb.jahugapy.ui.fragments.RootFragment;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.GPSTracker;
import py.com.ideasweb.jahugapy.utilities.SendMail;
import py.com.ideasweb.jahugapy.utilities.Share;
import py.com.ideasweb.jahugapy.utilities.Utilities;
import py.com.ideasweb.jahugapy.utilities.Validation;

public class MainActivity extends AppCompatActivity {

    private static final int INTERVALO = 2000; //2 segundos para salir
    private long tiempoPrimerClick;

    private Toolbar mToolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MaterialSearchView searchView;


    //correo
    EditText mensaje;
    EditText nombre;
    EditText cel;

    FloatingActionButton fab;

   /* //ubicacion
    GPSTracker gps;
    MaterialDialog addDialog;
    Button irMapa;

    //add form
    private String addNombre ="";
    private String addDir="";
    private String addCel="";
    private String addMsj="";*/



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        // se agrega el toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        //se centra el titulo
     //   mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.jahugapy_tollbar03));
      //  mTitle.setText(mToolbar.getTitle());

        //se oculta el title por defecto
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        nombre = (EditText) findViewById(R.id.correoNombre);
        cel = (EditText) findViewById(R.id.correoCel);
        mensaje = (EditText) findViewById(R.id.correoMensaje);

        setUpViewPager();
        agregarFAB();


        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int versionNumber = pinfo.versionCode;
        System.out.println("Codigo de version" + versionNumber);
        System.out.println("Utilities code" + Utilities.version_code);
        if( Utilities.version_code > versionNumber &&  Utilities.version_code > 0){
            new MaterialDialog.Builder(this)
                    .title("Estas utilizando una verion anterior. Desea actualizar?")
                    .titleColor(getResources().getColor(R.color.colorPrimary))
                    .icon(getResources().getDrawable(R.drawable.jahugapy_01))
                    .positiveText("Si")
                    .negativeText("Mas tarde")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(Config.API_DOWNLOAD_APK));
                            startActivity(intent);;
                        }
                    })
                    .show();
        }
         tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
             @Override
             public void onTabSelected(TabLayout.Tab tab) {
                 if(tab.getPosition() == 3){
                     fab.setVisibility(View.INVISIBLE);

                 }else {
                     fab.setVisibility(View.VISIBLE);
                 }
             }

             @Override
             public void onTabUnselected(TabLayout.Tab tab) {

             }

             @Override
             public void onTabReselected(TabLayout.Tab tab) {

             }
         });
    }


    // search
    /*public void searchCanchas(String param) {
        Cancha c = new Cancha();
        c.setDenominacion(param);
        Snackbar.make(getCurrentFocus(), "Query: " + param, Snackbar.LENGTH_LONG).show();


        CercanosFragment miFragment = CercanosFragment.newInstance();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.root_frame, new CiudadesFragment());
        fragmentTransaction.commit();
       // addFragment(miFragment); // todo: añade tu fragment con fragment manager
       // miFragment.refres();




    }*/

    //infla en el activity el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // menu de opciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mabout:
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                        .customView(R.layout.layout_acercade, wrapInScrollView)
                        .positiveText("OK")
                        .show();
                break;
            case R.id.mshare:
                Share.shareRedeSociales(this, Config.API_DOWNLOAD_APK, "JahugaPy - Encuentra las canchas deportivas cercanas a ti!!");
                break;
            case R.id.mcontact:
                Intent intent = new Intent(this, InfoActivity.class);
                intent.putExtra("fragment", "contacto");
                startActivity(intent);
                finish();
                break;
            case R.id.mfanPage:
                irFacebook();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private ArrayList<Fragment> agregarFragments() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new CercanosFragment());
        fragments.add(new RootFragment());
        fragments.add(new Root2Fragment());
        fragments.add(new Root3Fragment());
        return fragments;
    }

    private void setUpViewPager() {
        viewPager.setAdapter(new PageAdapter(getSupportFragmentManager(), agregarFragments()));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setText("Cercanos");
        tabLayout.getTabAt(1).setText("Ciudades");
        tabLayout.getTabAt(2).setText("Busqueda");
        tabLayout.getTabAt(3).setText("Mapa");
    }


    //agregar FAB
    public void agregarFAB() {
         fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, AgregarActivity.class);

               /* if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
                    Explode explode = new Explode();
                    explode.setDuration(1000);
                    getWindow().setExitTransition(new Explode());
                    startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(getParent(), view, "").toBundle());
                }else{*/
                    startActivity(intent);
              //  }

                finish();

            }
        });

    }

    // envio de correo electronico
    /*public void enviarCorreo() {

        if (checkValidation()) {
            sendEmail(direccion.getText().toString(), mensaje.getText().toString(), nombre.getText().toString(), cel.getText().toString());
            inicializarCampos();

        } else {
            Snackbar.make(getCurrentFocus(), "Mensaje no Enviado! Complete los campos", Snackbar.LENGTH_LONG).show();
            // nombre.requestFocus();
        }
    }*/


   /* private void sendEmail(String direccion, String mensaje, String nombre, String cel) {
        //Creating SendMail object
        SendMail sm = new SendMail(this, "", "Jahugapy", "Nombre del Local: " + nombre + "\n " + "Dirección: " + direccion + "\nCel : " + cel + "\nComentarios: " + mensaje, getCurrentFocus());
        //Executing sendmail to send email
        sm.execute();
    }

    private void inicializarCampos() {
        nombre.setText(null);
        direccion.setText(null);
        mensaje.setText(null);
        cel.setText(null);
        // nombre.requestFocus();
    }

    // validacion de formulario
    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText(nombre)) ret = false;
        if (!Validation.hasText(direccion)) ret = false;
        if (!Validation.hasText(cel)) ret = false;
        if (!Validation.hasText(mensaje)) ret = false;

        return ret;
    }*/

    //pulsar dos veces para cerrar app
    @Override
    public void onBackPressed(){
        if (searchView.isSearchOpen() ) {
            searchView.closeSearch();
        }else if( ReciclerViewPresenter.search){
            ReciclerViewPresenter.search = false;

            Intent mainIntent = new Intent(this, MainActivity.class);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mainIntent);
            finish();

        } else{

            if(viewPager.getCurrentItem()==0){

                if (tiempoPrimerClick + INTERVALO > System.currentTimeMillis()){
                    super.onBackPressed();
                    finish();
                    return;
                }else {
                    Toast.makeText(this, "Vuelva a presionar para cerrar", Toast.LENGTH_SHORT).show();
                }
                tiempoPrimerClick = System.currentTimeMillis();
            }else  if(viewPager.getCurrentItem()==1){
                if(Utilities.back){
                    Utilities.back = false;
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    Fragment cercanos = new CiudadesFragment();
                    trans.replace(R.id.root_frame, cercanos);
                    trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    trans.addToBackStack(null);
                    trans.commit();
                }else{
                 viewPager.setCurrentItem(0);
                }

            }else  if(viewPager.getCurrentItem()==2){
                if(Utilities.back){
                    Utilities.back = false;
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    Fragment cercanos = new PersonalizadoFragment();
                    trans.replace(R.id.root_frame2, cercanos);
                    trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    trans.addToBackStack(null);
                    trans.commit();
                }else{
                    viewPager.setCurrentItem(0);
                }

            }else {
                if(Utilities.back){
                    Utilities.back = false;
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    Fragment allmaps = new AllMapsFragment();
                    trans.replace(R.id.root_frame3, allmaps);
                    trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    trans.addToBackStack(null);
                    trans.commit();
                }else{
                    viewPager.setCurrentItem(0);
                }
            }
        }

    }

    public void irFacebook( ) {

        try {
            getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/169405010203582"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent=  new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/jahugapy"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkJaime(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://jaimeferreira91"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/jaimeferreira91"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkMarcos(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://marcos-cespedes-8aa92319"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/marcos-cespedes-8aa92319"));
            startActivity(intent);
        }
    }
    public void irLinkEmilio(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://juan-emilio-espinola-874716125"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/juan-emilio-espinola-874716125"));
            startActivity(intent);
        }
    }

}