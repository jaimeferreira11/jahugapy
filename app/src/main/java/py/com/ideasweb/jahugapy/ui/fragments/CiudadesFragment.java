package py.com.ideasweb.jahugapy.ui.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import py.com.ideasweb.jahugapy.Adapter.CiudadAdapater;
import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.api.ConstructorCiudades;
import py.com.ideasweb.jahugapy.pojo.CiudadRepo;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * A simple {@link Fragment} subclass.
 */
public class CiudadesFragment extends Fragment {

    private SwipeRefreshLayout refreshLayout;

    public CiudadesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ciudades, container, false);

        final ListView mLeadsList = (ListView) view.findViewById(R.id.frListCiudades);
        final ConstructorCiudades constructorCiudades = new ConstructorCiudades(getContext());

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.indicadorRefresh2);
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Utilities.isNetworkConnected(getContext())) {
                    try{
                        CiudadAdapater adapter = new CiudadAdapater(getContext(), constructorCiudades.ObtenerCiudades());
                        mLeadsList.setAdapter(adapter);
                    }catch (Exception e){
                    }

                }else{
                    Toast.makeText(getContext(), "Verfifique su conexion y recarge", Toast.LENGTH_LONG).show();
                }
                refreshLayout.setRefreshing(false);
            }
        });


        // Inicializar el adaptador con la fuente de datos.
        //CiudadAdapater adapter = new CiudadAdapater(getActivity(),
          //      CiudadRepo.cargarCiudades(getContext()));

        try{
            if(Utilities.isNetworkConnected(view.getContext())) {
                CiudadAdapater adapter = new CiudadAdapater(getContext(), constructorCiudades.ObtenerCiudades());
                //Relacionando la lista con el adaptador
                mLeadsList.setAdapter(adapter);
            }
        }catch (Exception e){

        }

        return view;
    }

}
