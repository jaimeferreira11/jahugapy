package py.com.ideasweb.jahugapy.ui.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.MessageFormat;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.config.Config;
import py.com.ideasweb.jahugapy.utilities.Share;
import py.com.ideasweb.jahugapy.utilities.Utilities;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Toolbar mToolbar;
    private TextView mTitle;
    private String latitud;
    private String longitud;
    private Button aceptar;
    private TextView nombre;
    private TextView direccion;
    Marker marcador;
   // datos del formulario add
    private String telefono;
    private String origen;
    private String addCel;
    private String addMsj;

    String paramatenciondesde ;
    String paramatencionhasta;
    Double parampreciohora;
    String paramdimenciones;
    String paramsuperficie ;
    String paramdeportestipo;
    Integer paramcantidadcanchas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        // se agrega el toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        //se centra el titulo
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        mTitle.setText(mToolbar.getTitle());
        //se oculta el title por defecto
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle param = getIntent().getExtras();
        String paramnombre = param.getString("nombre");
        String paramdireccion = param.getString("direccion");
        telefono = param.getString("telefono");
        origen = param.getString("origen");

        latitud = param.getString("latitud");
        longitud = param.getString("longitud");


        paramcantidadcanchas = param.getInt("cantidadcanchas");

        paramatenciondesde = param.getString("atenciondesde");
        paramatencionhasta = param.getString("atencionhasta");
        parampreciohora = param.getDouble("preciohora");
        paramdimenciones = param.getString("dimenciones");
        paramsuperficie = param.getString("superficie");
        paramdeportestipo = param.getString("deportestipo");



        nombre = (TextView)findViewById(R.id.tvMapNombre);
        direccion = (TextView)findViewById(R.id.tvMapDireccion);
        nombre.setText(paramnombre);
        direccion.setText(paramdireccion);






        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabMaps);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareUbicacion();
            }
        });




    }

    //infla en el activity el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // menu de opciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mabout:
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                        .customView(R.layout.layout_acercade, wrapInScrollView)
                        .positiveText("OK")
                        .show();
                break;
            case R.id.mshare:
                Share.shareRedeSociales(this, Config.API_DOWNLOAD_APK, "JahugaPy - Encuentra las canchas deportivas cercanas a ti!!");
                break;
            case R.id.mcontact:
                Intent intent = new Intent(this, InfoActivity.class);
                intent.putExtra("fragment", "contacto");
                startActivity(intent);
                finish();
                break;
            case android.R.id.home:
                if(origen.equals("cercanos")){
                    Intent i = new Intent(MapsActivity.this, Main2Activity.class);
                    startActivity(i);
                    finish();
                }else{
                    Intent i = new Intent(MapsActivity.this, perfilActivity.class);
                    i.putExtra("nombre", nombre.getText().toString());
                    i.putExtra("direccion", direccion.getText().toString());
                    i.putExtra("telefono", telefono);
                    i.putExtra("latitud", latitud);
                    i.putExtra("longitud", longitud);
                    i.putExtra("cantidadcanchas", paramcantidadcanchas);
                    i.putExtra("atenciondesde", paramatenciondesde);
                    i.putExtra("atencionhasta", paramatencionhasta);
                    i.putExtra("preciohora", parampreciohora);
                    i.putExtra("dimenciones", paramdimenciones);
                    i.putExtra("superficie", paramsuperficie);
                    i.putExtra("deportestipo", paramdeportestipo);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.mfanPage:
                irFacebook();
                break;


        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){

        if(keyCode == KeyEvent.KEYCODE_BACK){
            if(origen.equals("cercanos")){
                Intent intent = new Intent(MapsActivity.this, Main2Activity.class);
                startActivity(intent);
                finish();
            }else{
                Intent intent = new Intent(MapsActivity.this, perfilActivity.class);
                intent.putExtra("nombre", nombre.getText().toString());
                intent.putExtra("direccion", direccion.getText().toString());
                intent.putExtra("telefono", telefono);
                intent.putExtra("latitud", latitud);
                intent.putExtra("longitud", longitud);
                intent.putExtra("cantidadcanchas", paramcantidadcanchas);
                intent.putExtra("atenciondesde", paramatenciondesde);
                intent.putExtra("atencionhasta", paramatencionhasta);
                intent.putExtra("preciohora", parampreciohora);
                intent.putExtra("dimenciones", paramdimenciones);
                intent.putExtra("superficie", paramsuperficie);
                intent.putExtra("deportestipo", paramdeportestipo);
                startActivity(intent);
                finish();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public  void shareUbicacion(){
        /*Uri uri = Uri.parse("https://www.google.com/maps/@"+latitud+","+longitud+",15.5z/data=!10m1!1e2?hl=es-419");
        Share.shareRedeSociales(this, uri.toString(), nombre.getText().toString());*/


        String link = getLocationLink();

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, link);
        intent.setType("text/plain");
        startActivity(Intent.createChooser(intent, "Compartir Ubicacion de " + nombre.getText().toString()));

    }


    private String getLocationLink() {
        return MessageFormat.format("https://maps.google.com/?q={0},{1}("+nombre.getText().toString()+")",
                latitud, longitud);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(Double.parseDouble(latitud), Double.parseDouble(longitud));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        mMap.setMyLocationEnabled(true);
        marcador = mMap.addMarker(new MarkerOptions().position(sydney)
                  .icon(BitmapDescriptorFactory.fromResource(R.drawable.jahugapy_icon))
                .title(nombre.getText().toString())
                .snippet(direccion.getText().toString())
                );
        marcador.showInfoWindow();

        CameraPosition cameraPosition = CameraPosition.builder()
                .target(sydney)
                .zoom(15)
                .build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        /*mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Toast.makeText(getBaseContext(), String.valueOf(latLng.latitude) , Toast.LENGTH_LONG).show();

                mMap.clear();
                marcador = mMap.addMarker(new MarkerOptions().position(latLng)
                        .title(nombre.getText().toString())
                        .snippet(direccion.getText().toString())
                        .draggable(true));
            }
        });*/
    }

    public void irFacebook( ) {

        try {
            getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/169405010203582"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent=  new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/jahugapy"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkJaime(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://jaimeferreira91"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/jaimeferreira91"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkMarcos(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://marcos-cespedes-8aa92319"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/marcos-cespedes-8aa92319"));
            startActivity(intent);
        }
    }

    public void irLinkEmilio(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://juan-emilio-espinola-874716125"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/juan-emilio-espinola-874716125"));
            startActivity(intent);
        }
    }
}
