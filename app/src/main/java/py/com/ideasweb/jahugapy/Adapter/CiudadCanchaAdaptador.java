package py.com.ideasweb.jahugapy.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.ui.activities.MapsActivity;
import py.com.ideasweb.jahugapy.ui.activities.perfilActivity;
import py.com.ideasweb.jahugapy.utilities.CircleTransform;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * Created by jaime on 14/11/16.
 */
public class CiudadCanchaAdaptador extends RecyclerView.Adapter<CiudadCanchaAdaptador.CanchaViewHolder>{

    List<Cancha> canchas;
    Activity activity;

    public CiudadCanchaAdaptador(List<Cancha> canchas, Activity activity){
        this.canchas = canchas;
        this.activity = activity;
    }

    @Override
    public CanchaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_canchas, parent, false);
        return new CanchaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CanchaViewHolder holder, int position) {
        final Cancha cancha =  canchas.get(position);

        holder.tvNombreCV.setText(cancha.getDenominacion());
       // holder.imgFoto.setImageResource(cancha.getFoto());


        // si la direccion es muy larga, se recorta
        char[] arrayChar = cancha.getDireccion().toCharArray();
        if (arrayChar.length > 50) {
            String result = cancha.getDireccion().substring(0, 47) + "...";
            holder.direccion.setText(result);
        } else {
            holder.direccion.setText(cancha.getDireccion());
        }
        
        if(cancha.getCelular() == null || cancha.getCelular().equals("")){
            holder.telefono.setText(cancha.getTelefono());
        }else{
            holder.telefono.setText(cancha.getCelular());
        }


        holder.telefono.setText(cancha.getTelefono());

        int distancia = Utilities.getDistance(MiUbicacion.getLatitud(), MiUbicacion.getLongitud(),Double.parseDouble(cancha.getLatitud()),
                Double.parseDouble(cancha.getLongitud()) );
        holder.distancia.setText( Utilities.formatDistance(distancia));

        if(cancha.getProfile() != null){
            Picasso.with(activity).load(cancha.getProfile()).transform(new CircleTransform()).into(holder.imgFoto);
        }else{
            holder.imgFoto.setImageResource(R.drawable.jahugapy_01);
        }

        //boton de perfil
        holder.verperfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Ir a perfil", Snackbar.LENGTH_LONG)
                        .show();
                Intent intent = new Intent(view.getContext(), perfilActivity.class);
                intent.putExtra("nombre", cancha.getDenominacion());
                intent.putExtra("direccion", cancha.getDireccion());
                intent.putExtra("telefono", cancha.getTelefono());
                intent.putExtra("latitud", cancha.getLatitud());
                intent.putExtra("longitud", cancha.getLongitud());
                view.getContext().startActivity(intent);
                activity.finish();


            }
        });
        //boton de mapa
        holder.vermapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), MapsActivity.class);
                intent.putExtra("nombre", cancha.getDenominacion());
                intent.putExtra("direccion", cancha.getDireccion());
                intent.putExtra("telefono", cancha.getTelefono());
                intent.putExtra("latitud", cancha.getLatitud());
                intent.putExtra("longitud", cancha.getLongitud());
                intent.putExtra("origen", "cercanos");
                view.getContext().startActivity(intent);
                activity.finish();


            }
        });
    }

    @Override
    public int getItemCount() {
        return canchas.size();
    }

    public static class CanchaViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgFoto;
        private TextView tvNombreCV;
        private TextView direccion;
        private TextView telefono;
        private TextView distancia;
        private TextView vermapa;
        private TextView verperfil;

        public CanchaViewHolder(View itemView) {
            super(itemView);
            imgFoto = (ImageView) itemView.findViewById(R.id.iv_avatar);
            tvNombreCV = (TextView) itemView.findViewById(R.id.tv_nombre);
            direccion = (TextView) itemView.findViewById(R.id.tv_direccion);
            telefono = (TextView)itemView.findViewById(R.id.tv_telefono);
            distancia = (TextView) itemView.findViewById(R.id.tvDistancia);
            vermapa = (TextView) itemView.findViewById(R.id.tvMapa);
            verperfil = (TextView) itemView.findViewById(R.id.tvPerfil);
        }
    }
}
