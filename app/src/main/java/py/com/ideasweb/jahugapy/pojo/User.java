package py.com.ideasweb.jahugapy.pojo;

import android.net.Uri;

/**
 * Created by jaime on 13/01/17.
 */

public class User {
    private static Integer iduser;
    private static String iddevice;
    private static String email;
    private static Double latitud;
    private static Double longitud;
    private static String phone;

    private static String name;
    private static String photo;
    private static String googleid ;
    private static Boolean logueado = false;


    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        User.name = name;
    }

    public static String getPhoto() {
        return photo;
    }

    public static void setPhoto(String photo) {
        User.photo = photo;
    }

    public static String getGoogleid() {
        return googleid;
    }

    public static void setGoogleid(String googleid) {
        User.googleid = googleid;
    }

    public static Boolean getLogueado() {
        return logueado;
    }

    public static void setLogueado(Boolean logueado) {
        User.logueado = logueado;
    }

    public User() {
    }

    public static String getPhone() {
        return phone;
    }

    public static void setPhone(String phone) {
        User.phone = phone;
    }

    public static Integer getIduser() {
        return iduser;
    }

    public static void setIduser(Integer iduser) {
        User.iduser = iduser;
    }

    public static String getIddevice() {
        return iddevice;
    }

    public static void setIddevice(String iddevice) {
        User.iddevice = iddevice;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        User.email = email;
    }

    public static Double getLatitud() {
        return latitud;
    }

    public static void setLatitud(Double latitud) {
        User.latitud = latitud;
    }

    public static Double getLongitud() {
        return longitud;
    }

    public static void setLongitud(Double longitud) {
        User.longitud = longitud;
    }
}
