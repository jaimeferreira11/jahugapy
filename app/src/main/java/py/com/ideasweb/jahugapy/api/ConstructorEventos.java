package py.com.ideasweb.jahugapy.api;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import py.com.ideasweb.jahugapy.api.manager.Manager;
import py.com.ideasweb.jahugapy.pojo.Evento;
import py.com.ideasweb.jahugapy.pojo.Respuesta;

/**
 * Created by jaime on 20/02/17.
 */

public class ConstructorEventos {
    private Context context;

    public ConstructorEventos(Context context) {
        this.context = context;
    }


    public Respuesta ObtenerEventos() throws Exception {

        /*if(Utilities.isNetworkConnected(context)){
            ThreadManager threadManager =  new ThreadManager(context, "getOficinas" );
            threadManager.execute();
        }else {
            Toast.makeText(context, "Verfifique su conexion", Toast.LENGTH_LONG).show();
        }*/
        Manager manager =  new Manager();

        return manager.getEventos();
    }


    public List<Evento> ObtenerEventosDummy() {
        System.out.println("ConstructorEventos");
        List<Evento> list = new ArrayList<>();
        list.add(new Evento("evento 1"));
        list.add(new Evento("evento 1"));
        list.add(new Evento("evento 1"));
        list.add(new Evento("evento 1"));
        list.add(new Evento("evento 1"));


        return list;
    }
}
