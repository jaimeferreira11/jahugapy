package py.com.ideasweb.jahugapy.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.ui.fragments.ContactoFragment;
import py.com.ideasweb.jahugapy.utilities.SendMail;
import py.com.ideasweb.jahugapy.utilities.Share;
import py.com.ideasweb.jahugapy.utilities.Validation;


public class InfoActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextView mTitle;

    //contacto activity

    private EditText mensaje;
    private EditText nombre;
    private EditText documento;
    private EditText cel;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        // se agrega el toolbar
        // se agrega el toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        //se centra el titulo
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        mTitle.setText(mToolbar.getTitle());
        //se oculta el title por defecto
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle param = getIntent().getExtras();
        String extra = param.getString("fragment");


        //agregar el fragment a al actividad
        if(extra.equals("oficina")){



        }else if(extra.equals("quienessomos")){



        }else if(extra.equals("programas")){



        }else{
            ContactoFragment leadsFragment = (ContactoFragment)
                    getSupportFragmentManager().findFragmentById(R.id.fragmentContacto);

            if (leadsFragment == null) {
                leadsFragment = ContactoFragment.newInstance();
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.info_body, leadsFragment)
                        .commit();
            }
            mTitle.setText("Contacto");


        }



    }


    //infla en el activity el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // menu de opciones
    // menu de opciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mabout:
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .title("Acerca de nosotros")
                        .titleColor(getResources().getColor(R.color.colorPrimary))
                        .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                        .content("Ideas web... en desarrollo")
                        // agregar un layout
                        //.customView(R.layout.cardview_programas, wrapInScrollView)
                        .positiveText("OK")
                        .show();
                break;
            case R.id.mshare:
                Share.shareRedeSociales(this, "", "");
                break;
            case R.id.mcontact:
                Intent intent = new Intent(this, InfoActivity.class);
                intent.putExtra("fragment", "contacto");
                startActivity(intent);
                finish();
                break;
            case android.R.id.home:
                Intent i = new Intent(InfoActivity.this, Main2Activity.class);
                startActivity(i);
                finish();
                break;
            case R.id.mfanPage:
                irFacebook();
                break;


        }

        return super.onOptionsItemSelected(item);
    }

    public void irFacebook( ) {

        try {
            getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/169405010203582"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent=  new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/jahugapy"));
            startActivity(intent);
        }
    }



    //regresar a la actividad anterior al pulsar back
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){

        if(keyCode == KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(InfoActivity.this, Main2Activity.class);
            startActivity(intent);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }




    // envio de correo electronico
    public void enviarCorreo(View v) {

        mensaje = (EditText) findViewById(R.id.correoMensaje);
        nombre = (EditText) findViewById(R.id.correoNombre);
        cel =(EditText) findViewById(R.id.correoCel) ;
        view = v;

        if (checkValidation ()) {
                sendEmail(documento.getText().toString(), mensaje.getText().toString(), nombre.getText().toString(), cel.getText().toString());
                inicializarCampos();

        } else {
           // Toast.makeText(this, "Complete los campos..", Toast.LENGTH_SHORT).show();
           // nombre.requestFocus();
        }
    }


    private void sendEmail(String documento, String mensaje, String nombre, String cel) {
        //Creating SendMail object
        SendMail sm = new SendMail(this, "" , "Contacto Jahuga", mensaje+"\n \n"+nombre+"\nCI: "+documento+"\nCel: "+cel, view);
        //Executing sendmail to send email
        sm.execute();
    }

    private void inicializarCampos() {
        nombre.setText(null);
        documento.setText(null);
        mensaje.setText(null);
        cel.setText(null);
       // nombre.requestFocus();
    }



    ////*//**************************//*//



    // validacion de formulario
    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText((EditText) findViewById(R.id.correoNombre))) ret = false;
        if (!Validation.hasText((EditText) findViewById(R.id.correoMensaje))) ret = false;
        if (!Validation.hasText((EditText) findViewById(R.id.correoCel))) ret = false;

        return ret;
    }


    ///***********************////



}
