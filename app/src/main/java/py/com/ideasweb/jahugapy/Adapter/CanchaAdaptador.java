package py.com.ideasweb.jahugapy.Adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.ui.activities.MainActivity;
import py.com.ideasweb.jahugapy.ui.activities.MapsActivity;
import py.com.ideasweb.jahugapy.ui.activities.perfilActivity;
import py.com.ideasweb.jahugapy.utilities.CircleTransform;
import py.com.ideasweb.jahugapy.utilities.Utilities;

import static java.security.AccessController.getContext;

/**
 * Created by jaime on 14/11/16.
 */
public class CanchaAdaptador extends RecyclerView.Adapter<CanchaAdaptador.CanchaViewHolder>{

    List<Cancha> canchas;
    Activity activity;

    public CanchaAdaptador(List<Cancha> canchas, Activity activity){
        this.canchas = canchas;
        this.activity = activity;
    }

    @Override
    public CanchaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_canchas, parent, false);
        return new CanchaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CanchaViewHolder holder, int position) {
         final Cancha cancha =  canchas.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(view.getContext(), "Click", Toast.LENGTH_LONG).show();
                levantarMenuPopUp(view, cancha);
            }
        });

        holder.tvNombreCV.setText(cancha.getDenominacion());

        // si la direccion es muy larga, se recorta
        char[] arrayChar = cancha.getDireccion().toCharArray();
        if (arrayChar.length > 50) {
            String result = cancha.getDireccion().substring(0, 47) + "...";
            holder.direccion.setText(result);
        } else {
            holder.direccion.setText(cancha.getDireccion());
        }


        if(cancha.getCelular() == null || cancha.getCelular().equals("")){
            holder.telefono.setText(cancha.getTelefono());
        }else{
            holder.telefono.setText(cancha.getCelular());
        }

        if(cancha.getProfile() != null){
            Picasso.with(activity).load(cancha.getProfile()).transform(new CircleTransform()).into(holder.imgFoto);
        }else{
            holder.imgFoto.setImageResource(R.drawable.jahugapy_01);
        }

    //    int distancia = Utilities.getDistance(MiUbicacion.getLatitud(), MiUbicacion.getLongitud(),Double.parseDouble(cancha.getLatitud()),
     //           Double.parseDouble(cancha.getLongitud()) );
        holder.distancia.setText( Utilities.formatDistance(cancha.getDistancia()));

        //boton de perfil
        holder.verperfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), perfilActivity.class);
                intent.putExtra("nombre", cancha.getDenominacion());
                intent.putExtra("direccion", cancha.getDireccion());
                intent.putExtra("telefono", cancha.getTelefono());
                intent.putExtra("latitud", cancha.getLatitud());
                intent.putExtra("longitud", cancha.getLongitud());

                intent.putExtra("cantidadcanchas", cancha.getCantidadcanchas());
                intent.putExtra("atenciondesde", cancha.getAtenciondesde());
                intent.putExtra("atencionhasta", cancha.getAtencionhasta());
                intent.putExtra("preciohora", cancha.getPreciohora());
                intent.putExtra("dimenciones", cancha.getDimenciones());
                intent.putExtra("superficie", cancha.getSuperficie());
                intent.putExtra("deportestipo", cancha.getDeportestipo());
                intent.putExtra("diadesde", cancha.getDiadesde());
                intent.putExtra("diahasta", cancha.getDiahasta());
                intent.putExtra("comentario", cancha.getComentario());

                //nuevos
                intent.putExtra("preciohora2", cancha.getPreciohora2());
                intent.putExtra("celular", cancha.getCelular());
                intent.putExtra("dimension1", cancha.getDimension1());
                intent.putExtra("dimension2", cancha.getDimension2());
                intent.putExtra("superficie1", cancha.getSuperficie1());
                intent.putExtra("superficie2", cancha.getSuperficie2());
                intent.putExtra("deportestipo1", cancha.getDeportestipo1());
                intent.putExtra("deportestipo2", cancha.getDeportestipo2());
                if(cancha.getProfile() != null){
                    intent.putExtra("profile", cancha.getProfile());
                }


                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    Explode explode = new Explode();
                    explode.setDuration(1000);
                    activity.getWindow().setExitTransition(explode);
                    activity.startActivity(intent,
                            ActivityOptionsCompat.makeSceneTransitionAnimation(activity, view, "").toBundle());
                }else {
                    activity.startActivity(intent);
                }*/

                view.getContext().startActivity(intent);
                activity.finish();


            }
        });
        //boton de mapa
        holder.vermapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), MapsActivity.class);
                intent.putExtra("nombre", cancha.getDenominacion());
                intent.putExtra("direccion", cancha.getDireccion());
                intent.putExtra("telefono", cancha.getTelefono());
                intent.putExtra("latitud", cancha.getLatitud());
                intent.putExtra("longitud", cancha.getLongitud());
                intent.putExtra("origen", "cercanos");
                view.getContext().startActivity(intent);
                activity.finish();


            }
        });



    }

    @Override
    public int getItemCount() {
        return canchas.size();
    }

    public static class CanchaViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgFoto;
        private TextView tvNombreCV;
        private TextView direccion;
        private TextView telefono;
        private TextView distancia;
        private TextView vermapa;
        private TextView verperfil;

        public CanchaViewHolder(final View itemView) {
            super(itemView);
            imgFoto = (ImageView) itemView.findViewById(R.id.iv_avatar);
            tvNombreCV = (TextView) itemView.findViewById(R.id.tv_nombre);
            direccion = (TextView) itemView.findViewById(R.id.tv_direccion);
            telefono = (TextView)itemView.findViewById(R.id.tv_telefono);
            distancia = (TextView) itemView.findViewById(R.id.tvDistancia);
            vermapa = (TextView) itemView.findViewById(R.id.tvMapa);
            verperfil = (TextView) itemView.findViewById(R.id.tvPerfil);

        }
    }


    public void levantarMenuPopUp(final View view, final Cancha cancha){

        PopupMenu popupMenu = new PopupMenu(view.getContext(),view);
        popupMenu.getMenuInflater().inflate(R.menu.menu_popup, popupMenu.getMenu());
        popupMenu.setGravity(Gravity.RIGHT);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){

                    case R.id.mPerfil:

                        Intent intent = new Intent(view.getContext(), perfilActivity.class);
                        intent.putExtra("nombre", cancha.getDenominacion());
                        intent.putExtra("direccion", cancha.getDireccion());
                        intent.putExtra("telefono", cancha.getTelefono());
                        intent.putExtra("latitud", cancha.getLatitud());
                        intent.putExtra("longitud", cancha.getLongitud());

                        intent.putExtra("cantidadcanchas", cancha.getCantidadcanchas());
                        intent.putExtra("atenciondesde", cancha.getAtenciondesde());
                        intent.putExtra("atencionhasta", cancha.getAtencionhasta());
                        intent.putExtra("preciohora", cancha.getPreciohora());
                        intent.putExtra("dimenciones", cancha.getDenominacion());
                        intent.putExtra("superficie", cancha.getSuperficie());
                        intent.putExtra("deportestipo", cancha.getDeportestipo());

                        //nuevos
                        //nuevos
                        intent.putExtra("preciohora2", cancha.getPreciohora2());
                        intent.putExtra("celular", cancha.getCelular());
                        intent.putExtra("dimension1", cancha.getDimension1());
                        intent.putExtra("dimension2", cancha.getDimension2());
                        intent.putExtra("superficie1", cancha.getSuperficie1());
                        intent.putExtra("superficie2", cancha.getSuperficie2());
                        intent.putExtra("deportestipo1", cancha.getDeportestipo1());
                        intent.putExtra("deportestipo2", cancha.getDeportestipo2());


                        view.getContext().startActivity(intent);
                        activity.finish();
                        break;

                    case R.id.mMapa:
                        Intent i = new Intent(view.getContext(), MapsActivity.class);
                        i.putExtra("nombre", cancha.getDenominacion());
                        i.putExtra("direccion", cancha.getDireccion());
                        i.putExtra("telefono", cancha.getTelefono());
                        i.putExtra("latitud", cancha.getLatitud());
                        i.putExtra("longitud", cancha.getLongitud());
                        i.putExtra("origen", "cercanos");
                        view.getContext().startActivity(i);
                        activity.finish();
                        break;
                    case R.id.mLlamar:
                        String numero = "";
                        if(cancha.getCelular() != null){
                            numero = cancha.getCelular().replace("(", "");
                        }else{
                            numero = cancha.getTelefono().replace("(", "");
                        }
                        numero = numero.replace(")", "");
                        numero = numero.replace("-", "");
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:"+ numero));
                        view.getContext().startActivity(Intent.createChooser(callIntent, "Llamar a "+cancha.getDenominacion()));
                        break;
                }
                return true;
            }
        });

        popupMenu.show();
    }




}
