package py.com.ideasweb.jahugapy.pojo;

import java.sql.Time;

public class Cancha {

	private Integer idcancha;
	private String nombrefantasia;
	private String denominacion;
	private String documento;
	private String direccion;
	private String telefono;
	private Integer iddepartamento;
	private Integer iddistrito;
	private Integer idbarrio;
	private String latitud;
	private String longitud;
	private Boolean publicidad;
	private Double tarifapublicidad;
	private Integer cantidadcanchas;
	private String atenciondesde;
	private String atencionhasta;
	private Double preciohora;
	private String dimenciones;
	private String superficie;
	private String deportestipo;
	private Boolean estado;
	private Integer distancia;
	private String diadesde;
	private String diahasta;
    private String comentario;
	private String userlog;

	//nuevos
	private Double preciohora2;
	private Double  preciohora3;
	private  String celular ;
	private String dimension1;
	private String dimension2;
	private String dimension3 ;
	private String superficie1 ;
	private String superficie2;
	private String superficie3 ;
	private String deportestipo1 ;
	private String deportestipo2 ;
	private String deportestipo3;
	private String profile;


	public Cancha(Integer idcancha, String denominacion) {
		this.idcancha = idcancha;
		this.denominacion = denominacion;
	}

	public Cancha() {
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getDimension1() {
		return dimension1;
	}

	public void setDimension1(String dimension1) {
		this.dimension1 = dimension1;
	}

	public String getDimension2() {
		return dimension2;
	}

	public void setDimension2(String dimension2) {
		this.dimension2 = dimension2;
	}

	public String getDimension3() {
		return dimension3;
	}

	public void setDimension3(String dimension3) {
		this.dimension3 = dimension3;
	}

	public String getSuperficie1() {
		return superficie1;
	}

	public void setSuperficie1(String superficie1) {
		this.superficie1 = superficie1;
	}

	public String getSuperficie2() {
		return superficie2;
	}

	public void setSuperficie2(String superficie2) {
		this.superficie2 = superficie2;
	}

	public String getSuperficie3() {
		return superficie3;
	}

	public void setSuperficie3(String superficie3) {
		this.superficie3 = superficie3;
	}

	public String getDeportestipo1() {
		return deportestipo1;
	}

	public void setDeportestipo1(String deportestipo1) {
		this.deportestipo1 = deportestipo1;
	}

	public String getDeportestipo2() {
		return deportestipo2;
	}

	public void setDeportestipo2(String deportestipo2) {
		this.deportestipo2 = deportestipo2;
	}

	public String getDeportestipo3() {
		return deportestipo3;
	}

	public void setDeportestipo3(String deportestipo3) {
		this.deportestipo3 = deportestipo3;
	}

	public Double getPreciohora2() {
		return preciohora2;
	}

	public void setPreciohora2(Double preciohora2) {
		this.preciohora2 = preciohora2;
	}

	public Double getPreciohora3() {
		return preciohora3;
	}

	public void setPreciohora3(Double preciohora3) {
		this.preciohora3 = preciohora3;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}



	public String getUserlog() {
		return userlog;
	}

	public void setUserlog(String userlog) {
		this.userlog = userlog;
	}

	public String getComentario() {
        return comentario;
    }
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
	
	public Integer getCantidadcanchas() {
		return cantidadcanchas;
	}
	public void setCantidadcanchas(Integer cantidadcanchas) {
		this.cantidadcanchas = cantidadcanchas;
	}
	public String getAtenciondesde() {
		return atenciondesde;
	}
	public void setAtenciondesde(String atenciondesde) {
		this.atenciondesde = atenciondesde;
	}
	public String getAtencionhasta() {
		return atencionhasta;
	}
	public void setAtencionhasta(String atencionhasta) {
		this.atencionhasta = atencionhasta;
	}
	public Double getPreciohora() {
		return preciohora;
	}
	public void setPreciohora(Double preciohora) {
		this.preciohora = preciohora;
	}
	public String getDimenciones() {
		return dimenciones;
	}
	public void setDimenciones(String dimenciones) {
		this.dimenciones = dimenciones;
	}
	public String getSuperficie() {
		return superficie;
	}
	public void setSuperficie(String superficie) {
		this.superficie = superficie;
	}
	public String getDeportestipo() {
		return deportestipo;
	}
	public void setDeportestipo(String deportestipo) {
		this.deportestipo = deportestipo;
	}
	public Integer getIdcancha() {
		return idcancha;
	}
	public void setIdcancha(Integer idcancha) {
		this.idcancha = idcancha;
	}
	public String getNombrefantasia() {
		return nombrefantasia;
	}
	public void setNombrefantasia(String nombrefantasia) {
		this.nombrefantasia = nombrefantasia;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public Integer getIddepartamento() {
		return iddepartamento;
	}
	public void setIddepartamento(Integer iddepartamento) {
		this.iddepartamento = iddepartamento;
	}
	public Integer getIddistrito() {
		return iddistrito;
	}
	public void setIddistrito(Integer iddistrito) {
		this.iddistrito = iddistrito;
	}
	public Integer getIdbarrio() {
		return idbarrio;
	}
	public void setIdbarrio(Integer idbarrio) {
		this.idbarrio = idbarrio;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public Boolean getPublicidad() {
		return publicidad;
	}
	public void setPublicidad(Boolean publicidad) {
		this.publicidad = publicidad;
	}
	public Double getTarifapublicidad() {
		return tarifapublicidad;
	}
	public void setTarifapublicidad(Double tarifapublicidad) {
		this.tarifapublicidad = tarifapublicidad;
	}
	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Integer getDistancia() {
		return distancia;
	}

	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}

    public String getDiadesde() {
        return diadesde;
    }

    public void setDiadesde(String diadesde) {
        this.diadesde = diadesde;
    }

    public String getDiahasta() {
        return diahasta;
    }

    public void setDiahasta(String diahasta) {
        this.diahasta = diahasta;
    }

	//to display object as a string in spinner
	@Override
	public String toString() {
		return denominacion;
	}
	
	
}
