package py.com.ideasweb.jahugapy.ui.fragments;


import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.ui.presentador.IRecyclerViewPresenter;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllMapsFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;

    public AllMapsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_maps, container, false);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.allmap)).getMapAsync(this);

        return view;
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);


        for ( Cancha cancha : ReciclerViewPresenter.canchasCercanas ) {
            LatLng position = new LatLng(Double.parseDouble(cancha.getLatitud()), Double.parseDouble(cancha.getLongitud()));

            Marker maker = mMap.addMarker(new MarkerOptions().position(position)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.jahugapy_icon))
                    .title(cancha.getDenominacion())
                    .snippet(cancha.getDireccion())
            );
        }
        /*for (int i = 0; i < ReciclerViewPresenter.canchasCercanas.size(); i++) {
            Cancha cancha =  ReciclerViewPresenter.canchasCercanas.get(i);
            LatLng position = new LatLng(Double.parseDouble(cancha.getLatitud()), Double.parseDouble(cancha.getLongitud()));

            Marker maker = mMap.addMarker(new MarkerOptions().position(position)
                             .icon(BitmapDescriptorFactory.fromResource(R.drawable.jahugapy_icon))
                            .title(cancha.getDenominacion())
                            .snippet(cancha.getDireccion())
            );

        }*/

        LatLng miubicaion = new LatLng(MiUbicacion.getLatitud(), MiUbicacion.getLongitud());

        CameraPosition cameraPosition = CameraPosition.builder()
                .target(miubicaion)
                .zoom(12)
                .build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(miubicaion));
    }
}
