package py.com.ideasweb.jahugapy.api.manager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.EOFException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.Respuesta;
import py.com.ideasweb.jahugapy.ui.activities.AgregarActivity;
import py.com.ideasweb.jahugapy.ui.activities.Main2Activity;
import py.com.ideasweb.jahugapy.ui.activities.MainActivity;
import py.com.ideasweb.jahugapy.ui.activities.SplashScreen;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.Utilities;


/**
 * Created by jaime on 14/12/16.
 */

public class ThreadManager extends AsyncTask<Void,Void,Respuesta> {

    //Declaring Variables
    private Context context;
    private String metodo;
    private Cancha cancha;
    private String msgOk;
    private View view;
    //Progressdialog to show while sending email
    private ProgressDialog progressDialog;


    //Class Constructor
    public ThreadManager(Context context, String metodo, Cancha cancha, String msgOk, View view){

        this.context = context;
        this.metodo = metodo;
        this.cancha = cancha;
        this.msgOk = msgOk;
        this.view = view;

    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        System.out.println(metodo);
        if (metodo.equals("getCanchas") || metodo.equals("registerUser") || metodo.equals("asistencia") || metodo.equals("eliminarasistencia")) {

        }else{
            progressDialog = ProgressDialog.show(context,"Enviando datos","Favor aguarde...",false,false);

        }

    }

    @Override
    protected void onPostExecute(Respuesta respuesta) {
        super.onPostExecute(respuesta);

        if (respuesta.getEstado() != null) {
            if (metodo.equals("insertCancha")){

                progressDialog.dismiss();
            }

            if (respuesta.getEstado().equals("OK")) {
                if (metodo.equals("getCanchas")) {

                    ReciclerViewPresenter.canchasCercanas = (List<Cancha>) respuesta.getDatos();
                    Utilities.ordenarLista(ReciclerViewPresenter.canchasCercanas);

                    Intent mainIntent = new Intent(context, Main2Activity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(mainIntent);
                    ((SplashScreen)context).finish();


                }else if (metodo.equals("insertCancha")){

                    new MaterialDialog.Builder(context)
                            .title(msgOk)
                            .content("Gracias por Contribuir! \n Se verificaran los datos antes de aparecer en la lista.")
                            .titleColor(context.getResources().getColor(R.color.colorPrimary))
                            .icon(context.getResources().getDrawable(R.drawable.checked_48))
                            .positiveText("OK")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    Intent intent = new Intent(context, Main2Activity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(intent);
                                    ((AgregarActivity)view.getContext()).finish();

                                    //inicializarFormulario(view);
                                }
                            })
                            .show();

                }else if (metodo.equals("registerUser")){

                }else if (metodo.equals("getCanchasByParam")) {
                    progressDialog.dismiss();

                    ReciclerViewPresenter.searchCanchas = (List<Cancha>) respuesta.getDatos();
                    Utilities.ordenarLista(ReciclerViewPresenter.searchCanchas);



                   /* Intent mainIntent = new Intent(context, MainActivity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(mainIntent);
                    ((SplashScreen)context).finish();*/


                }
            } else {
                if(metodo.equals("asistencia") || !metodo.equals("eliminarasistencia")){
                    new MaterialDialog.Builder(context)
                            .title("Error")
                            .content("Intente mas tarde")
                            .titleColor(context.getResources().getColor(R.color.colorPrimary))
                            .icon(context.getResources().getDrawable(R.drawable.cancel_48))
                            .content(respuesta.getError())
                            .positiveText("OK")
                            .show();
                }
            }

        }else if (respuesta.getEstado() == "ERROR") {

            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            new MaterialDialog.Builder(context)
                    .title("Error")
                    .content("Intente mas tarde")
                    .titleColor(context.getResources().getColor(R.color.colorPrimary))
                    .icon(context.getResources().getDrawable(R.drawable.cancel_48))
                    .content(respuesta.getError())
                    .positiveText("OK")
                    .show();
        }

    }



    @Override
    protected Respuesta doInBackground(Void... params) {

        Respuesta respuesta = new Respuesta();
        try {
            Manager manager = new Manager();


            if (metodo.equals("getCanchas")) {
                respuesta = manager.getCanchas();

            }else if(metodo.equals("insertCancha")){
                respuesta = manager.insertCancha(cancha);
            }else if (metodo.equals("registerUser")){
                manager.registerUser(context);
            }else if(metodo.equals("getCanchasByParam")){
                respuesta = manager.getCanchasByParam(cancha);
            }else if (metodo.equals("asistencia")){
                respuesta = manager.asistirEvento(msgOk, "S");
            }else if (metodo.equals("eliminarasistencia")){
                respuesta = manager.asistirEvento(msgOk, "N");
            }


        }catch (ConnectException c){
            if(metodo.equals("getCanchas")){
                Intent mainIntent = new Intent(context, Main2Activity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(mainIntent);
                ((SplashScreen)context).finish();
            }else{
                c.printStackTrace();
//                Toast.makeText(context,"Servicio No disponible", Toast.LENGTH_LONG);
            }


        }catch (SocketTimeoutException e){
            if(metodo.equals("getCanchas")){
                Intent mainIntent = new Intent(context, Main2Activity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(mainIntent);
                ((SplashScreen)context).finish();
            }else{
                e.printStackTrace();
                Toast.makeText(context,"Servicio No disponible", Toast.LENGTH_LONG);
            }
        }catch (EOFException e){
            if(metodo.equals("getCanchas")){
                Intent mainIntent = new Intent(context, Main2Activity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(mainIntent);
                ((SplashScreen)context).finish();
            }else{
                e.printStackTrace();
                Toast.makeText(context,"Servicio No disponible", Toast.LENGTH_LONG);
            }
        }catch (Exception e){
            e.printStackTrace();
//            Toast.makeText(context, "Error " + e.getClass().getCanonicalName(), Toast.LENGTH_LONG);
        }

        return respuesta;
    }


    public void inicializarFormulario(View view){

        EditText direccion = (EditText) view.findViewById(R.id.addDireccion);
        direccion.setText("");
        EditText comentarios = (EditText) view.findViewById(R.id.addComentarios);
        EditText  nombre= (EditText) view.findViewById(R.id.AddNombre);
        EditText cel = (EditText) view.findViewById(R.id.addCelular);
        EditText hora = (EditText) view.findViewById(R.id.tvHora);
        EditText cantidad = (EditText) view.findViewById(R.id.addCantidad);
        EditText precio = (EditText) view.findViewById(R.id.addPrecio);
        comentarios.setText(null);
        nombre.setText(null);
        cel.setText(null);
        hora.setText(null);
        cantidad.setText(null);
        precio.setText(null);

        CheckBox d5v5 = (CheckBox) view.findViewById(R.id.add5v5);
        d5v5.setChecked(false);
        CheckBox d6v6= (CheckBox) view.findViewById(R.id.add6v6);
        d6v6.setChecked(false);
        EditText otrodimen = (EditText) view.findViewById(R.id.addOtroDimen);
        otrodimen.setText(null);
        //superficie
        CheckBox sintetico= (CheckBox) view.findViewById(R.id.addSintetico);
        sintetico.setChecked(false);
        CheckBox natural= (CheckBox) view.findViewById(R.id.addNatural);
        natural.setChecked(false);
        CheckBox piso= (CheckBox) view.findViewById(R.id.addPiso);
        piso.setChecked(false);
        EditText otrosuper = (EditText) view.findViewById(R.id.addOtroSuperficie);
        otrosuper.setText(null);
        //deporte
        CheckBox futbol= (CheckBox) view.findViewById(R.id.addFutbol);
        futbol.setChecked(false);
        CheckBox volley= (CheckBox) view.findViewById(R.id.addVolley);
        volley.setChecked(false);
        CheckBox padel= (CheckBox) view.findViewById(R.id.addPadel);
        padel.setChecked(false);
        EditText otrodeporte = (EditText) view.findViewById(R.id.addOtroDeporte);
        otrodeporte.setText(null);
        //
        CheckBox checkin= (CheckBox) view.findViewById(R.id.checkin);
        checkin.setChecked(false);

    }



}
