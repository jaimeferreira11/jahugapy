package py.com.ideasweb.jahugapy.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.api.manager.ThreadManager;
import py.com.ideasweb.jahugapy.config.Config;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.utilities.CircleTransform;
import py.com.ideasweb.jahugapy.utilities.PrefManager;
import py.com.ideasweb.jahugapy.utilities.Share;
import py.com.ideasweb.jahugapy.utilities.Utilities;

public class PerfilEventoActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    TextView mTitle;
    private ImageView img;
    private TextView tvnombre ;
    private TextView tvcancha ;
    private TextView tvdistancia ;
    private TextView tvcomentario ;
    private TextView tvusuario ;
    private TextView tvcorreo ;
    private TextView tvfaltante ;
    private TextView tvfecha ;
    private TextView tvhora ;
    private TextView tvdimension ;
    private TextView tvsuperficie ;
    private TextView tvdeporte ;
    private Button asistir;
    private Boolean ban = false;
    private Boolean ban2 = false;
    private Boolean ban3 = false;
    int idevento = 0;
    private PrefManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_evento);

        // se agrega el toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        //se centra el titulo
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        mTitle.setText(mToolbar.getTitle());
        //se oculta el title por defecto
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pref = new PrefManager(getApplicationContext());
        Bundle param = getIntent().getExtras();

        String descripcion = param.getString("descripcion");
        String cancha = param.getString("cancha");
        String profile = param.getString("profile");
        String usuario = param.getString("usuario");
        String correo = param.getString("correo");
        int total = param.getInt("total");
        int asistencia = param.getInt("asistencia");
        String fecha = param.getString("fecha");
        String horadesde = param.getString("horadesde");
        String horahasta = param.getString("horahasta");
        String day = param.getString("day");
        int dimension = param.getInt("dimension");
        int superficie = param.getInt("superficie");
        int deporte = param.getInt("deporte");
        String comentario = param.getString("comentario");
        int distancia = param.getInt("distancia");
        idevento = param.getInt("id");

        System.out.println(total);
        System.out.println(asistencia);
        final int faltante = total - asistencia;

        img = (ImageView) findViewById(R.id.imgPerEvento);
        if(profile != ""){
            Picasso.with(getApplicationContext()).load(profile).transform(new CircleTransform()).into(img);
        }
        tvnombre = (TextView) findViewById(R.id.nombrePerEvento);
        tvnombre.setText(descripcion);
        tvcancha = (TextView) findViewById(R.id.canchaPerEvento);
        tvcancha.setText(cancha);
        tvdistancia = (TextView) findViewById(R.id.distanciaPerEvento);
        tvdistancia.setText(Utilities.formatDistance(distancia));
        tvcomentario = (TextView) findViewById(R.id.event_comentario);
        tvcomentario.setText(comentario);
        tvusuario = (TextView) findViewById(R.id.namePerEvento);
        tvusuario.setText(usuario);
        tvcorreo = (TextView) findViewById(R.id.emailPerEvento);
        tvcorreo.setText(correo);
        tvfaltante = (TextView) findViewById(R.id.faltaPerEvento);
        tvfaltante.setText("Faltan " + String.valueOf(faltante) + " jugadores");
        tvfecha = (TextView) findViewById(R.id.fechaPerEvento);
        tvfecha.setText(day.toUpperCase() + " " + fecha.substring(0,5));
        tvhora = (TextView) findViewById(R.id.horaPerEvento);
        tvhora.setText(horadesde+" a " + horahasta + " hs");
        tvdimension = (TextView) findViewById(R.id.dimensionPerEvento);
        if(dimension==1){
            tvdimension.setText("5vs5");

        }else if(dimension == 2){
            tvdimension.setText("6vs6");

        }else if(dimension ==3){
            tvdimension.setText("7vs7");

        }else if (dimension == 4){
            tvdimension.setText("11vs11");

        }else{
            tvdimension.setText("2vs2");

        }

        tvsuperficie = (TextView) findViewById(R.id.superficiePerEvento);
        if(superficie==1){

            tvsuperficie.setText("SINTETICO");
        }else if(superficie==2){

            tvsuperficie.setText("PISO");
        }else if(superficie==3){

            tvsuperficie.setText("NATURAL");
        }else if (superficie==4){

            tvsuperficie.setText("PARQUET");
        }

        tvdeporte = (TextView) findViewById(R.id.deportePerEvento);
        if(deporte == 1){

            tvdeporte.setText("FUTBOL");
        }else if(deporte == 2){

            tvdeporte.setText("PADEL");
        }else if(deporte == 3){

            tvdeporte.setText("VOLLEY");
        }else if (deporte==4){

            tvdeporte.setText("STREET SOCCER");
        }else{

            tvdeporte.setText("PIKKI VOLLEY");
        }
        asistir = (Button)findViewById(R.id.asistir);

        //verifica que ya le haya dado asistir a este evento
        String parts[] = pref.getEvento().split("-");
        for (int j = 0; j < parts.length ; j++) {
            if(parts[j].equals(String.valueOf(idevento))){
                asistir.setText("Ya no Asistire");
                asistir.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                ban2 = true;
            }
        }
        asistir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ban2){
                    if(ban){

                        Snackbar.make(v, "No asistiras al evento", Snackbar.LENGTH_SHORT).show();
                        asistir.setText(" Asistire");
                        asistir.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        tvfaltante.setText("Faltan " + String.valueOf(faltante) + " jugadores");
                        ban = false;
                    }else{
                        Snackbar.make(v, "Asistiras al evento", Snackbar.LENGTH_SHORT).show();

                        asistir.setText("Ya no Asistire");
                        asistir.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        int aux = faltante - 1;
                        tvfaltante.setText("Faltan " + String.valueOf(aux) + " jugadores");
                        ban = true;
                    }
                }else{
                    asistir.setText(" Asistire");
                    asistir.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    tvfaltante.setText("Faltan " + String.valueOf(faltante+1) + " jugadores");
                    ban2 =false;
                    ban3 = true;
                }
            }
        });


    }


    //infla en el activity el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("onDestroy");
        System.out.println("la bandera es: " + ban);
        if(ban && !ban2){
            pref.setEvento(idevento);
            ThreadManager threadManager =  new ThreadManager(getApplicationContext(),"asistencia", new Cancha(), String.valueOf(idevento) , getCurrentFocus());
            threadManager.execute();
        }else if(!ban && !ban2 && ban3){
            pref.removeFav(idevento);
            ThreadManager threadManager =  new ThreadManager(getApplicationContext(),"eliminarasistencia", new Cancha(), String.valueOf(idevento) , getCurrentFocus());
            threadManager.execute();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("onPause");
    }

    // menu de opciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mabout:
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                        .customView(R.layout.layout_acercade, wrapInScrollView)
                        .positiveText("OK")
                        .show();
                break;
            case R.id.mshare:
                Share.shareRedeSociales(this, Config.API_DOWNLOAD_APK, "JahugaPy - Encuentra las canchas deportivas cercanas a ti!!");
                break;
            case R.id.mcontact:
                Intent intent = new Intent(this, InfoActivity.class);
                intent.putExtra("fragment", "contacto");
                startActivity(intent);
                finish();
                break;
            case android.R.id.home:
                Intent i = new Intent(PerfilEventoActivity.this, EventosActivity.class);
                startActivity(i);
                finish();

                break;
            case R.id.mfanPage:
                irFacebook();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){

        if(keyCode == KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(PerfilEventoActivity.this, EventosActivity.class);
            startActivity(intent);
            finish();

        }
        return super.onKeyDown(keyCode, event);
    }

    public void irFacebook( ) {

        try {
            getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/169405010203582"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent=  new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/jahugapy"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkJaime(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://jaimeferreira91"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/jaimeferreira91"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkMarcos(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://marcos-cespedes-8aa92319"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/marcos-cespedes-8aa92319"));
            startActivity(intent);
        }
    }

    public void irLinkEmilio(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://juan-emilio-espinola-874716125"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/juan-emilio-espinola-874716125"));
            startActivity(intent);
        }
    }
}
