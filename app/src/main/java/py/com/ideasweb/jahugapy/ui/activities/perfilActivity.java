package py.com.ideasweb.jahugapy.ui.activities;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.provider.ContactsContract.CommonDataKinds;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;


import java.text.DecimalFormat;
import java.util.HashMap;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.config.Config;
import py.com.ideasweb.jahugapy.utilities.CircleTransform;
import py.com.ideasweb.jahugapy.utilities.Share;
import py.com.ideasweb.jahugapy.utilities.Utilities;

public class perfilActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{
    private Toolbar mToolbar;
    TextView mTitle;
    TextView nombre;
    TextView direccion;
    TextView telefono;
    TextView lineabaja;

    TextView cantidadcanchas;
    TextView horario;
    TextView preciohora;
    TextView dimenciones;
    TextView superficie;
    TextView deportestipo;

    ImageView foto;
    String latitud;
    String longitud;
    String paramatenciondesde;
    String paramatencionhasta;
    Integer paramcantidadcanchas;
    Double parampreciohora;

    //galeria
    SliderLayout sliderLayout;
    HashMap<String,String> Hash_file_maps ;
    MaterialDialog dialog;
    LinearLayout sinfotos;
    TextView titulogaleria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        // se agrega el toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        //se centra el titulo
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        mTitle.setText(mToolbar.getTitle());
        //se oculta el title por defecto
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Imagen circukar
        CircularImageView circularImageView = (CircularImageView) findViewById(R.id.imagCircularPerfil);
        circularImageView.setBorderColor(getResources().getColor(R.color.icons));
        circularImageView.setBorderWidth(5);
        circularImageView.addShadow();
        circularImageView.setShadowRadius(15);
        circularImageView.setShadowColor(getResources().getColor(R.color.colorPrimaryLight));

        //tectview
        cantidadcanchas = (TextView) findViewById(R.id.tvcantidadcancha);
         horario = (TextView) findViewById(R.id.tvhoraatencion);
         preciohora = (TextView) findViewById(R.id.tvpreciohora);
         dimenciones = (TextView) findViewById(R.id.tvdimension);
         superficie = (TextView) findViewById(R.id.tvsuperficie);
         deportestipo = (TextView) findViewById(R.id.tvdeportetipo);


        Bundle param = getIntent().getExtras();

        String paramnombre = param.getString("nombre");
        String paramdireccion = param.getString("direccion");
         latitud = param.getString("latitud");
         longitud = param.getString("longitud");
        String paramtelefono = param.getString("telefono");
        int img = param.getInt("imagen");

        //nuevos
        Double paramprecio2 = param.getDouble("preciohora2");
        String paramcelular = param.getString("celular");
        String paramdimension1 = param.getString("dimension1");
        String paramdimension2 = param.getString("dimension2");
        String paramsuperficie1 = param.getString("superficie1");
        String paramsuperficie2 = param.getString("superficie2");
        String paramdeportestipo1 = param.getString("deportestipo1");
        String paramdeportestipo2 = param.getString("deportestipo2");


        //nuevos parametros bundle
         paramcantidadcanchas = param.getInt("cantidadcanchas");
        cantidadcanchas.setText(paramcantidadcanchas.toString());
         paramatenciondesde = param.getString("atenciondesde");
         paramatencionhasta = param.getString("atencionhasta");
        horario.setText(paramatenciondesde + " a "+ paramatencionhasta);
         parampreciohora = param.getDouble("preciohora");
        preciohora.setText(Utilities.toStringFromDoubleWithFormat(parampreciohora));
        String paramdimenciones = param.getString("dimenciones");
        dimenciones.setText(paramdimension1.toString());
        String paramsuperficie = param.getString("superficie");
        superficie.setText(paramsuperficie1.toString());
        String paramdeportestipo = param.getString("deportestipo");
        deportestipo.setText(paramdeportestipo1.toString());


        if(paramprecio2 != 0){
            preciohora.setText(preciohora.getText() + " - "+Utilities.toStringFromDoubleWithFormat(paramprecio2));
        }
        if(paramdimension2 != null){
            dimenciones.setText(dimenciones.getText() + " - " + paramdimension2.toString());
        }
        if(paramsuperficie2 != null){
            superficie.setText(superficie.getText() + " - " + paramsuperficie2.toString());
        }
        if(paramdeportestipo2 != null){
            deportestipo.setText(deportestipo.getText() + " - " + paramdeportestipo2.toString());
        }

        nombre = (TextView)findViewById(R.id.tvPerfilNombre);
        direccion =(TextView)findViewById(R.id.tvPerfilDireccion);
        telefono = (TextView)findViewById(R.id.tvPerfilTelefono);
        lineabaja = (TextView)findViewById(R.id.tvPerfilLineaBaja);
        foto = (ImageView) findViewById(R.id.imagCircularPerfil);

        nombre.setText(paramnombre);
        direccion.setText(paramdireccion);
        if(paramcelular != null){
            String htmlString="<u>"+paramcelular.toString()+"</u>";
            telefono.setText(Html.fromHtml(htmlString));
        }
        if(paramtelefono != null){
            String htmlString2="<u>"+paramtelefono.toString()+"</u>";
            lineabaja.setText(Html.fromHtml(htmlString2));
        }
        String profile;
        if(param.getString("profile") != null){
            profile = param.getString("profile");
            Picasso.with(getApplicationContext()).load(profile).transform(new CircleTransform()).into(foto);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabPerfil);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareContact();
            }
        });


        //Galeria de Fotos
        boolean wrapInScrollView = true;
        dialog =  new MaterialDialog.Builder(this)
                .titleColor(getResources().getColor(R.color.colorPrimary))
                .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                // agregar un layout
                .customView(R.layout.layout_galeria, wrapInScrollView)
                .positiveText("OK")
                .show();
        dialog.dismiss();
        View viewGaleria = dialog.getCustomView();
        Hash_file_maps = new HashMap<String, String>();

        sliderLayout = (SliderLayout) viewGaleria.findViewById(R.id.slider);
        sinfotos = (LinearLayout) viewGaleria.findViewById(R.id.sinFotos);
        titulogaleria = (TextView) viewGaleria.findViewById(R.id.tvTittleGaleria);
        titulogaleria.setText("GALERIA DE " + paramnombre);

        Hash_file_maps.put("29", "http://www.fundacionparaguaya.org.py/hercules-rest/public/image/opt-images-jahugapy-cancha-profiles/29.png");
        Hash_file_maps.put("30", "http://www.fundacionparaguaya.org.py/hercules-rest/public/image/opt-images-jahugapy-cancha-profiles/30.png");
        Hash_file_maps.put("32", "http://www.fundacionparaguaya.org.py/hercules-rest/public/image/opt-images-jahugapy-cancha-profiles/32.png");
        Hash_file_maps.put("35", "http://www.fundacionparaguaya.org.py/hercules-rest/public/image/opt-images-jahugapy-cancha-profiles/35.png");
        Hash_file_maps.put("36", "http://www.fundacionparaguaya.org.py/hercules-rest/public/image/opt-images-jahugapy-cancha-profiles/36.png");

        for(String name : Hash_file_maps.keySet()){

            TextSliderView textSliderView = new TextSliderView(perfilActivity.this);
            textSliderView
                    .description(name)
                    .image(Hash_file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(3000);
        sliderLayout.addOnPageChangeListener(this);

    }

    //infla en el activity el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // menu de opciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mabout:
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                        .customView(R.layout.layout_acercade, wrapInScrollView)
                        .positiveText("OK")
                        .show();
                break;
            case R.id.mshare:
                Share.shareRedeSociales(this, Config.API_DOWNLOAD_APK, "JahugaPy - Encuentra las canchas deportivas cercanas a ti!!");
                break;
            case R.id.mcontact:
                Intent intent = new Intent(this, InfoActivity.class);
                intent.putExtra("fragment", "contacto");
                startActivity(intent);
                finish();
                break;
            case android.R.id.home:
                    Intent i = new Intent(perfilActivity.this, Main2Activity.class);
                    startActivity(i);
                    finish();

                break;
            case R.id.mfanPage:
                irFacebook();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){

        if(keyCode == KeyEvent.KEYCODE_BACK){
                Intent intent = new Intent(perfilActivity.this, Main2Activity.class);
                startActivity(intent);
                finish();

        }
        return super.onKeyDown(keyCode, event);
    }


    public void llamar(View v) {
        String numero = telefono.getText().toString().replace("(", "");
        numero = numero.replace(")", "");
        numero = numero.replace("-", "");
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:"+ numero));
        startActivity(Intent.createChooser(callIntent, "Llamar a "+nombre.getText().toString()));

    }

    public void llamarLineaBaja(View v) {
        String numero = lineabaja.getText().toString().replace("(", "");
        numero = numero.replace(")", "");
        numero = numero.replace("-", "");
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:"+ numero));
        startActivity(Intent.createChooser(callIntent, "Llamar a "+nombre.getText().toString()));

    }




    public void verMapa(View view){
        Intent intent = new Intent(view.getContext(), MapsActivity.class);
        intent.putExtra("nombre", nombre.getText().toString());
        intent.putExtra("direccion", direccion.getText().toString());
        intent.putExtra("telefono", telefono.getText().toString());
        intent.putExtra("latitud", latitud);
        intent.putExtra("longitud", longitud);
        intent.putExtra("origen", "perfil");
        intent.putExtra("cantidadcanchas",paramcantidadcanchas);
        intent.putExtra("atenciondesde", paramatenciondesde);
        intent.putExtra("atencionhasta", paramatencionhasta);
        intent.putExtra("preciohora", parampreciohora);
        intent.putExtra("dimenciones", dimenciones.getText().toString());
        intent.putExtra("superficie", superficie.getText().toString());
        intent.putExtra("deportestipo", deportestipo.getText().toString());

        view.getContext().startActivity(intent);
        finish();

    }

    public void verGaleria(View view){

        if(Hash_file_maps.size() == 0){
            sinfotos.setVisibility(View.VISIBLE);

        }
         dialog.show();
    }

    public  void guardarContact(View view){

        Intent addContactIntent = new Intent(Contacts.Intents.Insert.ACTION, Contacts.People.CONTENT_URI);
        addContactIntent.putExtra(Contacts.Intents.Insert.NAME, nombre.getText().toString()); // an example, there is other data available
        addContactIntent.putExtra(Contacts.Intents.Insert.PHONE, telefono.getText().toString()); // an example, there is other data available
        startActivity(addContactIntent);


    }

    public  void shareContact(){

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, nombre.getText().toString());
        intent.putExtra(Intent.EXTRA_TEXT, telefono.getText().toString());
        startActivity(Intent.createChooser(intent, "Compartir Numero de  "+nombre.getText().toString()));

    }

    public void irFacebook( ) {

        try {
            getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/169405010203582"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent=  new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/jahugapy"));
            startActivity(intent);
        }
    }


    //Link redes sociales
    public void irLinkJaime(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://jaimeferreira91"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/jaimeferreira91"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkMarcos(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://marcos-cespedes-8aa92319"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/marcos-cespedes-8aa92319"));
            startActivity(intent);
        }
    }

    public void irLinkEmilio(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://juan-emilio-espinola-874716125"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/juan-emilio-espinola-874716125"));
            startActivity(intent);
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
