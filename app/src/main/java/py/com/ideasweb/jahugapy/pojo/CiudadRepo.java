package py.com.ideasweb.jahugapy.pojo;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedInputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import py.com.ideasweb.jahugapy.api.ConstructorCanchas;
import py.com.ideasweb.jahugapy.api.manager.Manager;
import py.com.ideasweb.jahugapy.config.Config;
import py.com.ideasweb.jahugapy.utilities.JsonHelper;

import static py.com.ideasweb.jahugapy.config.Config.context;


/**
 * Created by jaime on 18/11/16.
 */

public class CiudadRepo {

    private static ArrayList<Ciudad> ciudades = new ArrayList<>();

    public static ArrayList<Ciudad> cargarCiudades(Context context) throws Exception {
        if(ciudades.size() == 0){
            /*ciudades.add(new Ciudad(1, "Asuncion", 10));
            ciudades.add(new Ciudad(2, "Fernando de la Mora",8));
            ciudades.add(new Ciudad(3, "San Lorenzo",15));
            ciudades.add(new Ciudad(4, "Capiata",5));
            ciudades.add(new Ciudad(5, "Lambare",9));*/
            getCiudades();
        }

        return ciudades;
    }

    public static void getCiudades(){

            Manager manager = new Manager();
            Respuesta respuesta = new Respuesta();
            try {
                respuesta = manager.getCiudades();
                if(respuesta.getEstado().equals("OK")){
                    ciudades = (ArrayList<Ciudad>) respuesta.getDatos();
                }else{

                }

            }catch (Exception e){
                e.printStackTrace();
            }

    }






}
