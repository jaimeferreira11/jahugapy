package py.com.ideasweb.jahugapy.pojo;

/**
 * Created by jaime on 01/12/16.
 */

public class Ciudad {
    private int iddistrito;
    private String descripcion;
    private int cantidad;
    private int iddepartamento;
    private Boolean estado;

    public Ciudad() {
    }

    public Ciudad(int iddistrito, String descripcion, int cantidad) {
        this.iddistrito = iddistrito;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
    }

    public Ciudad(int iddistrito, String descripcion) {
        this.iddistrito = iddistrito;
        this.descripcion = descripcion;
    }

    public int getIddistrito() {
        return iddistrito;
    }

    public void setIddistrito(int iddistrito) {
        this.iddistrito = iddistrito;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getIddepartamento() {
        return iddepartamento;
    }

    public void setIddepartamento(int iddepartamento) {
        this.iddepartamento = iddepartamento;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return descripcion;
    }
}
