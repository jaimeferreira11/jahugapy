package py.com.ideasweb.jahugapy;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import py.com.ideasweb.jahugapy.pojo.User;

/**
 * Created by root on 29/12/16.
 */

public class NotificacionIDTokenService extends FirebaseInstanceIdService {

    private static final String TAG = "FIREBASE_TOKEN";

    @Override
    public void onTokenRefresh() {
        //super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken();
        registrarToken(token);

    }

    public void  registrarToken(String token){

        User.setIddevice(token);

    }

}
