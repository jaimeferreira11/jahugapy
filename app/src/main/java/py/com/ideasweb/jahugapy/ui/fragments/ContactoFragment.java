package py.com.ideasweb.jahugapy.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import py.com.ideasweb.jahugapy.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactoFragment extends Fragment {


    public ContactoFragment() {
        // Required empty public constructor
    }

    public static ContactoFragment newInstance() {
        ContactoFragment fragment = new ContactoFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contacto, container, false);
    }

}
