package py.com.ideasweb.jahugapy.api;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.api.manager.Manager;
import py.com.ideasweb.jahugapy.api.manager.ThreadManager;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.Respuesta;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * Created by jaime on 05/12/16.
 */

public class ConstructorCanchas {
    private Context context;

    public ConstructorCanchas(Context context) {
        this.context = context;
    }

    //obtiene del manager la lista de canchas cercanas
    public Respuesta ObtenerCanchasCercanas() throws Exception {

        /*if(Utilities.isNetworkConnected(context)){
            ThreadManager threadManager =  new ThreadManager(context, "getOficinas" );
            threadManager.execute();
        }else {
            Toast.makeText(context, "Verfifique su conexion", Toast.LENGTH_LONG).show();
        }*/
        Manager manager =  new Manager();

        return manager.getCanchas();
    }

    //obtiene del manager la lista de canchas por id ciudad
    public Respuesta ObtenerCanchasCiudad(String  idciudad) throws Exception{
        Manager manager =  new Manager();
        return manager.getCanchasByCiudad(idciudad);
    }


    //obtiene del manager la lista de canchas por id ciudad
    public Respuesta ObtenerCanchasbyFiltro(String  json) throws Exception{
        Manager manager =  new Manager();
        return manager.getCanchasByFiltro(json);
    }

}
