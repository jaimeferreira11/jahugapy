package py.com.ideasweb.jahugapy.ui.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import py.com.ideasweb.jahugapy.Adapter.CanchaAdaptador;
import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.api.manager.Manager;
import py.com.ideasweb.jahugapy.api.manager.ThreadManager;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.pojo.Respuesta;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.GPSTracker;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * A simple {@link Fragment} subclass.
 */
public class CercanosFragment extends Fragment implements IReciclerView {

    private RecyclerView canchasCercanas;
     SwipeRefreshLayout refreshLayout;
    private GPSTracker gps;
    private Context context;
    private MaterialSearchView searchView;
    private LinearLayout sindatos;
    private Respuesta respuesta = new Respuesta();

    public CercanosFragment() {
        // Required empty public constructor
    }

    public static CercanosFragment newInstance() {
        CercanosFragment fragment = new CercanosFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cercanos, container, false);
        context = view.getContext();
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.indicadorRefresh);

        sindatos =(LinearLayout) view.findViewById(R.id.sinDatos);

        //prueba


        searchView = (MaterialSearchView) getActivity().findViewById(R.id.search_view) ;
        searchView.setVoiceSearch(false);
        searchView.setCursorDrawable(R.drawable.custom_cursor);
        // searchView.setSuggestions(getResources().getStringArray(R.array.query_suggestions));
        searchView.setEllipsize(true);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //  Snackbar.make(getView(), "Query: " + query, Snackbar.LENGTH_LONG).show();
               // searchCanchas(query);
                refresh(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });




        gps = new GPSTracker(view.getContext());
        canchasCercanas = (RecyclerView) view.findViewById(R.id.rvCanchasCercanas);


        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Utilities.isNetworkConnected(context)) {
                    if (MiUbicacion.getLongitud() == null) {
                        new MaterialDialog.Builder(context)
                                .cancelable(false)
                                .title("No se encontro su ubicacion")
                                .titleColor(getResources().getColor(R.color.colorPrimary))
                                .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                                .content("Desea realizar la busqueda con la ultima ubicacion registrada?")
                                // agregar un layout
                                .positiveText("OK")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        Log.i("OK", "Pulso OK");
                                        SharedPreferences sharedPreferences = context.getSharedPreferences("ubicacion", Context.MODE_PRIVATE);
                                        MiUbicacion.setLatitud(Double.parseDouble(sharedPreferences.getString("latitud", "0.0")));
                                        MiUbicacion.setLongitud(Double.parseDouble(sharedPreferences.getString("longitud", "0.0")));
                                        new ReciclerViewPresenter(CercanosFragment.this, context, "cercanos", "", true);

                                    }
                                })
                                .negativeText("Cancelar")
                                .show();
                    } else {
                        gps.getLocation();
                        new ReciclerViewPresenter(CercanosFragment.this, getContext(), "cercanos", "", true);
                        ThreadManager threadManager2 =  new ThreadManager(getContext(), "registerUser", new Cancha(), "", getView());
                        threadManager2.execute();

                    }
                }else{
                    Toast.makeText(context, "Verfifique su conexion y recarge", Toast.LENGTH_LONG).show();
                }
                refreshLayout.setRefreshing(false);
            }
        });

        //pregunta si el gps esta activado
        if (!gps.canGetLocation() && MiUbicacion.getLongitud() == null) {
            gps.showSettingsAlert();
            Log.i("gps desactivado", String.valueOf(MiUbicacion.getLatitud()));
        } else {

        }

        Bundle b = getArguments();
        if(b != null){
            if(Utilities.isNetworkConnected(context)){
                System.out.println("Inicio busca ciudad?" );
                String idciudad = b.getString("idciudad");
                new ReciclerViewPresenter(CercanosFragment.this, getContext(), "ciudades", idciudad, false);
            }

        }else{
            if(Utilities.isNetworkConnected(context) && MiUbicacion.getLatitud() != null){
                System.out.println("Inicio busca canchas" );
                new ReciclerViewPresenter(CercanosFragment.this, getContext(), "cercanos", "", false);
            }else {
                Toast.makeText(context, "Verfifique su conexion y recarge", Toast.LENGTH_LONG).show();
            }


        }

        return view;
    }



    public void refresh(String param){
        System.out.println("Buscando.......");
        Cancha cancha = new Cancha();
        cancha.setDenominacion(param.trim());
        refreshLayout.setRefreshing(true);
        clearData();
        try {
            Manager manager = new Manager();
            respuesta = manager.getCanchasByParam(cancha);
            ReciclerViewPresenter.searchCanchas.clear();
            ReciclerViewPresenter.search = true;
            ReciclerViewPresenter.searchCanchas = (List<Cancha>) respuesta.getDatos();
        }catch (Exception e){
            refreshLayout.setRefreshing(false);
        }
        new ReciclerViewPresenter(CercanosFragment.this, getContext(), "search", "", false);
        if(ReciclerViewPresenter.searchCanchas.size()== 0){
            sindatos.setVisibility(View.VISIBLE);
        }
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        gps.stopUsingGPS();
    }

    @Override
    public void onResume() {
        super.onResume();
        gps = new GPSTracker(context);

    }


    @Override
    public void generarLineaLayoutVertical() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        canchasCercanas.setLayoutManager(llm);
    }

    @Override
    public CanchaAdaptador crearAdaptador(List<Cancha> canchas ) {
        CanchaAdaptador adaptador = new CanchaAdaptador(canchas, getActivity());
        return adaptador;
    }

    @Override
    public void inicializarAdaptadorRV(CanchaAdaptador adapatador) {
        canchasCercanas.setAdapter(adapatador);

    }

    public void clearData() {
       // mylist.removeAll(mylist);
        CanchaAdaptador adaptador = new CanchaAdaptador(new ArrayList<Cancha>() , getActivity());
        canchasCercanas.setAdapter(adaptador);


    }

}
