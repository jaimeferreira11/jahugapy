package py.com.ideasweb.jahugapy.ui.presentador;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import py.com.ideasweb.jahugapy.api.ConstructorCanchas;
import py.com.ideasweb.jahugapy.api.ConstructorEventos;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.Evento;
import py.com.ideasweb.jahugapy.pojo.Respuesta;
import py.com.ideasweb.jahugapy.ui.activities.EventosActivity;
import py.com.ideasweb.jahugapy.ui.fragments.IReciclerView;
import py.com.ideasweb.jahugapy.ui.fragments.IReciclerViewEvento;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * Created by jaime on 05/12/16.
 */

public class ReciclerViewPresenter implements IRecyclerViewPresenter {

    private IReciclerView iReciclerView;
    private IReciclerViewEvento iReciclerViewEvento;
    private Context context;
    private ConstructorCanchas constructorCanchas;
    private ConstructorEventos constructorEventos;
    private Respuesta respuesta;
    public static List<Cancha> canchasCiudad= new ArrayList<Cancha>();
    public static List<Cancha> canchasCercanas = new ArrayList<Cancha>();
    public static List<Cancha> searchCanchas = new ArrayList<Cancha>();
    public static List<Cancha> filtroCanchas = new ArrayList<Cancha>();
    public static List<Evento> listeventos = new ArrayList<>();
    public static boolean search = false;

    public ReciclerViewPresenter(IReciclerView iReciclerView, Context context, String busqueda, String idciudad, Boolean reload) {
        this.iReciclerView = iReciclerView;
        this.context = context;
        if(busqueda.equals("cercanos")){
            obtenerCanchasCercanasApi(reload);
        }else if(busqueda.equals("search")){
            System.out.println("ReciclerViewPresenter search");
            searchCanchasApi(reload);

        }else if(busqueda.equals("filtro")){
            System.out.println("ReciclerViewPresenter filtro");
            filtroCanchasApi(idciudad, reload);

        }else {
            System.out.println("ReciclerViewPresenter ciudades");
            obtenerCanchasCiudadApi(idciudad, reload);
        }
    }

    public ReciclerViewPresenter(IReciclerViewEvento iReciclerViewEvento, Context context, boolean reload) {
        this.iReciclerViewEvento = iReciclerViewEvento;
        this.context = context;
        System.out.println("ReciclerViewPresenter eventos");
        obtenerEventosApi(reload);
    }


    @Override
    public void obtenerCanchasCercanasApi(Boolean reload) {

        if(canchasCercanas.size() == 0 || reload){
            constructorCanchas =  new ConstructorCanchas(context);
            try {
                respuesta = constructorCanchas.ObtenerCanchasCercanas();
                if(respuesta.getEstado().equals("OK")){
                    canchasCercanas = (List<Cancha>) respuesta.getDatos();
                    Utilities.ordenarLista(canchasCercanas);

                }else{
                    Toast.makeText(context, respuesta.getError() , Toast.LENGTH_LONG).show();
                }
                mostrarCanchasCercanasRV();
            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            mostrarCanchasCercanasRV();
        }

    }

    @Override
    public void mostrarCanchasCercanasRV() {
        System.out.println("Mostrando canchas cercanas");
        iReciclerView.inicializarAdaptadorRV(iReciclerView.crearAdaptador(canchasCercanas));
        iReciclerView.generarLineaLayoutVertical();

    }

    @Override
    public void obtenerCanchasCiudadApi(String idciudad, Boolean reload) {

            constructorCanchas =  new ConstructorCanchas(context);
            try {
                respuesta = constructorCanchas.ObtenerCanchasCiudad(idciudad);
                if(respuesta.getEstado().equals("OK")){
                    canchasCiudad = (List<Cancha>) respuesta.getDatos();
                    Utilities.ordenarLista(canchasCiudad);

                }else{
                    Toast.makeText(context, respuesta.getError() , Toast.LENGTH_LONG).show();
                }
                mostrarCanchasCiudadRV();
            }catch (Exception e){
                e.printStackTrace();
            }

    }

    @Override
    public void mostrarCanchasCiudadRV() {
        iReciclerView.inicializarAdaptadorRV(iReciclerView.crearAdaptador(canchasCiudad));
        iReciclerView.generarLineaLayoutVertical();
    }

    @Override
    public void searchCanchasApi(Boolean reload) {

        if(searchCanchas != null){
            Utilities.ordenarLista(searchCanchas);
        }
        searchCanchasCercanasRV();


    }

    @Override
    public void searchCanchasCercanasRV() {
        iReciclerView.inicializarAdaptadorRV(iReciclerView.crearAdaptador(searchCanchas));
        iReciclerView.generarLineaLayoutVertical();
    }

    @Override
    public void filtroCanchasApi(String json , Boolean reload) {
        constructorCanchas =  new ConstructorCanchas(context);
        try {
            respuesta = constructorCanchas.ObtenerCanchasbyFiltro(json);
            if(respuesta.getEstado().equals("OK")){
                filtroCanchas = (List<Cancha>) respuesta.getDatos();
                Utilities.ordenarLista(filtroCanchas);
                filtroCanchasCercanasRV();
            }else{
                Toast.makeText(context, respuesta.getError() , Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void filtroCanchasCercanasRV() {

        iReciclerView.inicializarAdaptadorRV(iReciclerView.crearAdaptador(filtroCanchas));
        iReciclerView.generarLineaLayoutVertical();

    }

    @Override
    public void obtenerEventosApi(Boolean reload) {
        constructorEventos =  new ConstructorEventos(context);
        //Dummy
       /* listeventos = constructorEventos.ObtenerEventosDummy();
        mostartEventosRV();*/
        //api
        try {
            respuesta = constructorEventos.ObtenerEventos();
            if(respuesta.getEstado().equals("OK")){
                listeventos = (List<Evento>) respuesta.getDatos();
                Utilities.ordenarEventos(listeventos);

            }else{
                Toast.makeText(context, respuesta.getError() , Toast.LENGTH_LONG).show();
            }
            mostartEventosRV();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void mostartEventosRV() {
        iReciclerViewEvento.inicializarAdaptadorRV(iReciclerViewEvento.crearAdaptador(listeventos));
        iReciclerViewEvento.generarLineaLayoutVertical();
    }
}
