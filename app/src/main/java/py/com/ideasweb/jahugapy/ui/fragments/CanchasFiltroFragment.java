package py.com.ideasweb.jahugapy.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import py.com.ideasweb.jahugapy.Adapter.CanchaAdaptador;
import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * A simple {@link Fragment} subclass.
 */
public class CanchasFiltroFragment extends Fragment implements IReciclerView{

    private RecyclerView canchasCercanas;
    String json;
    private SwipeRefreshLayout refreshLayout;
    private TextView parametros;
    String valor1;
    String param1="";
    String valor2;
    String param2="";
    String valor3;
    String param3="";
    private LinearLayout sindatos;


    public CanchasFiltroFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_canchas_filtro, container, false);
        canchasCercanas = (RecyclerView) view.findViewById(R.id.rvFiltroCancha);
        parametros = (TextView) view.findViewById(R.id.tvFiltroParametros) ;
        sindatos =(LinearLayout) view.findViewById(R.id.sinDatosFiltro);
        Bundle b = getArguments();
        if(b != null){
            json = b.getString("json");

            if(Utilities.isNetworkConnected(getContext()) && MiUbicacion.getLatitud() != null) {
                new ReciclerViewPresenter(CanchasFiltroFragment.this, getContext(), "filtro", json, false);
            }else{
                Toast.makeText(getContext(), "Verfifique su conexion y recarge", Toast.LENGTH_LONG).show();
            }
            if(! b.getString("valor1").equals("")){
                param1= b.getString("param1");
            }
            if(! b.getString("valor2").equals("")){
                param2= "," +b.getString("param2");
            }
            if(! b.getString("valor3").equals("")){
                param3= "," + b.getString("param3");
            }

        }
        parametros.setText(param1 +" "+ param2+" "+param3);

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.indicadorRefreshFiltro);
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //new ReciclerViewPresenter(CanchasFiltroFragment.this, getContext(), "ciudades", json, true);
                refreshLayout.setRefreshing(false);
            }
        });
        if(ReciclerViewPresenter.filtroCanchas.size()== 0){
            sindatos.setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public void generarLineaLayoutVertical() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        canchasCercanas.setLayoutManager(llm);
    }

    @Override
    public CanchaAdaptador crearAdaptador(List<Cancha> canchas) {
        CanchaAdaptador adaptador = new CanchaAdaptador(ReciclerViewPresenter.filtroCanchas, getActivity());
        System.out.print("Size filtro :" + ReciclerViewPresenter.filtroCanchas);
        return adaptador;
    }

    @Override
    public void inicializarAdaptadorRV(CanchaAdaptador adapatador) {
        canchasCercanas.setAdapter(adapatador);
    }
}
