package py.com.ideasweb.jahugapy.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.pojo.Evento;
import py.com.ideasweb.jahugapy.ui.activities.EventosActivity;
import py.com.ideasweb.jahugapy.ui.activities.PerfilEventoActivity;
import py.com.ideasweb.jahugapy.utilities.CircleTransform;


/**
 * Created by jaime on 18/02/17.
 */

public class EventosAdaptador  extends RecyclerView.Adapter<EventosAdaptador.EventoViewHolder>{

    List<Evento> eventos;
    Activity activity;

    public EventosAdaptador(List<Evento> eventos, Activity activity){
        this.eventos = eventos;
        this.activity = activity;

    }

    @Override
    public EventosAdaptador.EventoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_eventos, parent, false);
        return new EventosAdaptador.EventoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final EventosAdaptador.EventoViewHolder holder, int position) {
        final Evento evento =  eventos.get(position);

        Locale spanishLocale=new Locale("es", "ES");
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        final Time horahasta = new Time( evento.getHorahasta().getTime());
        final Time horadesde = new Time( evento.getHoradesde().getTime());


        final String datestring = formatter.format(evento.getFechaevento());
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE", spanishLocale); // the day of the week spelled out completely
        final String day = simpleDateformat.format(evento.getFechaevento());

        System.out.println("fecha : " + evento.getFechaevento());
        System.out.println("hora" +evento.getHorahasta());



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), PerfilEventoActivity.class);

                intent.putExtra("descripcion", evento.getDescripcion());
                intent.putExtra("cancha", evento.getCancha().getDenominacion());
                intent.putExtra("profile", evento.getCancha().getProfile());
                intent.putExtra("usuario", evento.getUserapp().getName());
                intent.putExtra("correo", evento.getUserapp().getEmail());
                intent.putExtra("total", evento.getTotalinvitados());
                intent.putExtra("asistencia", evento.getAsistencia());

                intent.putExtra("fecha", datestring);
                intent.putExtra("day", day.toUpperCase());
                intent.putExtra("horadesde", horadesde.toString().substring(0,5));
                intent.putExtra("horahasta",horahasta.toString().substring(0,5));
                intent.putExtra("dimension", evento.getDimension());
                intent.putExtra("superficie", evento.getSuperficie());
                intent.putExtra("deporte", evento.getDeporte());
                intent.putExtra("comentario", evento.getComentarios());
                intent.putExtra("distancia", evento.getDistancia());
                intent.putExtra("id", evento.getIdevento());

                view.getContext().startActivity(intent);
                ((EventosActivity)view.getContext()).finish();

            }
        });

        char[] arrayChar = evento.getDescripcion().toCharArray();
        if (arrayChar.length > 30) {
            String result = evento.getDescripcion().substring(0, 27) + "...";
            holder.nombre.setText(result.toUpperCase());
        } else {
            holder.nombre.setText(evento.getDescripcion().toUpperCase());
        }
        holder.cancha.setText(evento.getCancha().getDenominacion());
        /*holder.cancha.setText(evento.getCancha().getDenominacion());

       *//* // si la direccion es muy larga, se recorta
        char[] arrayChar = cancha.getDireccion().toCharArray();
        if (arrayChar.length > 50) {
            String result = cancha.getDireccion().substring(0, 47) + "...";
            holder.direccion.setText(result);
        } else {
        }
*/



        String fe =  datestring + " : " +horadesde.toString().substring(0,5)+" a "+horahasta.toString().substring(0,5) + " hs.";
        holder.fecha.setText(fe);


        if(evento.getCancha().getProfile() != null){
            Picasso.with(activity).load(evento.getCancha().getProfile() ).transform(new CircleTransform()).into(holder.imgFoto);
        }else{
            holder.imgFoto.setImageResource(R.drawable.jahugapy_01);
        }


    }

    @Override
    public int getItemCount() {
        return eventos.size();
    }

    public static class EventoViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgFoto;
        private TextView nombre;
        private TextView cancha;
        private TextView fecha;


        public EventoViewHolder(final View itemView) {
            super(itemView);
            imgFoto = (ImageView) itemView.findViewById(R.id.iv_evento);
            nombre = (TextView) itemView.findViewById(R.id.tv_evento);
            cancha = (TextView) itemView.findViewById(R.id.tv_cancha);
            fecha = (TextView)itemView.findViewById(R.id.tv_fecha);


        }
    }



}
