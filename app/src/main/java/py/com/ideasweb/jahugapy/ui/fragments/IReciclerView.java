package py.com.ideasweb.jahugapy.ui.fragments;

import java.util.List;

import py.com.ideasweb.jahugapy.Adapter.CanchaAdaptador;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.Respuesta;

/**
 * Created by jaime on 05/12/16.
 */

public interface IReciclerView {

    public void generarLineaLayoutVertical();
    public CanchaAdaptador crearAdaptador(List<Cancha> canchas);
    public void inicializarAdaptadorRV(CanchaAdaptador adapatador);

}
