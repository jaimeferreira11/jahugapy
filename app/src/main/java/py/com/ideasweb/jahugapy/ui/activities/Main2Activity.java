package py.com.ideasweb.jahugapy.ui.activities;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;

import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import py.com.ideasweb.jahugapy.Adapter.DrawerAdapter;
import py.com.ideasweb.jahugapy.Adapter.PageAdapter;
import py.com.ideasweb.jahugapy.BuildConfig;
import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.api.manager.ThreadManager;
import py.com.ideasweb.jahugapy.config.Config;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.User;
import py.com.ideasweb.jahugapy.ui.Utils.ItemClickSupport;
import py.com.ideasweb.jahugapy.ui.element.DrawerItem;
import py.com.ideasweb.jahugapy.ui.fragments.AllMapsFragment;
import py.com.ideasweb.jahugapy.ui.fragments.CercanosFragment;
import py.com.ideasweb.jahugapy.ui.fragments.CiudadesFragment;
import py.com.ideasweb.jahugapy.ui.fragments.PersonalizadoFragment;
import py.com.ideasweb.jahugapy.ui.fragments.Root2Fragment;
import py.com.ideasweb.jahugapy.ui.fragments.Root3Fragment;
import py.com.ideasweb.jahugapy.ui.fragments.RootFragment;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.CircleTransform;
import py.com.ideasweb.jahugapy.utilities.Share;
import py.com.ideasweb.jahugapy.utilities.Utilities;

public class Main2Activity extends AppCompatActivity  {

    private Toolbar mToolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    FrameLayout statusBar;
    RelativeLayout relativeLayoutScrollViewChild;
    ScrollView scrollViewNavigationDrawerContent;
    ViewTreeObserver viewTreeObserverNavigationDrawerScrollView;
    ViewTreeObserver.OnScrollChangedListener onScrollChangedListener;
    RecyclerView recyclerViewDrawer1, recyclerViewDrawer2, recyclerViewDrawerSettings;
    RecyclerView.Adapter drawerAdapter1, drawerAdapter2,  drawerAdapterSettings;
    ArrayList<DrawerItem> drawerItems1, drawerItems2, drawerItemsSettings;
    float drawerHeight, scrollViewHeight;
    LinearLayoutManager linearLayoutManager, linearLayoutManager2, linearLayoutManager3, linearLayoutManagerSettings;
    ItemClickSupport itemClickSupport1, itemClickSupport2, itemClickSupport3, itemClickSupportSettings;
    int colorPrimary, textColorPrimary, colorControlHighlight, colorBackground;

    //del main
    private static final int INTERVALO = 2000; //2 segundos para salir
    private long tiempoPrimerClick;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MaterialSearchView searchView;


    //correo
    EditText direccion;
    EditText mensaje;
    EditText nombre;
    EditText cel;

    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        //se centra el titulo
        //   mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.jahugapy_tollbar03));
        //  mTitle.setText(mToolbar.getTitle());

        //se oculta el title por defecto
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //statusBar = (FrameLayout) findViewById(R.id.statusBar);
        
        setupNavigationDrawer();
        //registrar el ususario
        registerUser();


        //del main
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        nombre = (EditText) findViewById(R.id.correoNombre);
        cel = (EditText) findViewById(R.id.correoCel);
        mensaje = (EditText) findViewById(R.id.correoMensaje);

        setUpViewPager();
        agregarFAB();

        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int versionNumber = pinfo.versionCode;
        System.out.println("Codigo de version" + versionNumber);
        System.out.println("Utilities code" + Utilities.version_code);
        if( Utilities.version_code > versionNumber &&  Utilities.version_code > 0){
            new MaterialDialog.Builder(this)
                    .title("Estas utilizando una verion anterior. Desea actualizar?")
                    .titleColor(getResources().getColor(R.color.colorPrimary))
                    .positiveText("Si")
                    .negativeText("Mas tarde")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(Config.API_DOWNLOAD_APK));
                            startActivity(intent);;
                        }
                    })
                    .show();
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (Build.VERSION.SDK_INT > 21) {
                    tab.getIcon().setTint(getResources().getColor(R.color.icons));
                }
                if(tab.getPosition() == 3){
                    fab.setVisibility(View.INVISIBLE);

                }else {
                    fab.setVisibility(View.VISIBLE);
                }

                for (int i = 0; i < recyclerViewDrawer1.getChildCount(); i++) {
                    ImageView imageViewDrawerItemIcon = (ImageView) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.imageViewDrawerItemIcon);
                    TextView textViewDrawerItemTitle = (TextView) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.textViewDrawerItemTitle);
                    LinearLayout linearLayoutItem = (LinearLayout) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.linearLayoutItem);
                    if (i == tab.getPosition()) {
                        imageViewDrawerItemIcon.setColorFilter(colorPrimary);
                        textViewDrawerItemTitle.setTextColor(colorPrimary);
                        linearLayoutItem.setBackgroundColor(colorControlHighlight);

                    } else {
                        imageViewDrawerItemIcon.setColorFilter(textColorPrimary);
                        textViewDrawerItemTitle.setTextColor(textColorPrimary);
                        linearLayoutItem.setBackgroundColor(colorBackground);

                    }
                }


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (Build.VERSION.SDK_INT > 21) {
                    tab.getIcon().setTint(getResources().getColor(R.color.colorPrimaryLight));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    //infla en el activity el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // menu de opciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mabout:
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                        .customView(R.layout.layout_acercade, wrapInScrollView)
                        .positiveText("OK")
                        .show();
                break;
            case R.id.mshare:
                Share.shareRedeSociales(this, Config.API_DOWNLOAD_APK, "JahugaPy - Encuentra las canchas deportivas cercanas a ti!!");
                break;
            case R.id.mcontact:
                Intent intent = new Intent(this, InfoActivity.class);
                intent.putExtra("fragment", "contacto");
                startActivity(intent);
                finish();
                break;
            case R.id.mfanPage:
                irFacebook();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Fragment> agregarFragments() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new CercanosFragment());
        fragments.add(new RootFragment());
        fragments.add(new Root2Fragment());
        fragments.add(new Root3Fragment());
        return fragments;
    }

    private void setUpViewPager() {
        viewPager.setAdapter(new PageAdapter(getSupportFragmentManager(), agregarFragments()));
        tabLayout.setupWithViewPager(viewPager);
        //tabLayout.getTabAt(0).setText("Cercanos");
        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.marker_48_blank));

        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.map_marker_48));
        if (Build.VERSION.SDK_INT > 21) {
            tabLayout.getTabAt(1).getIcon().setTint(getResources().getColor(R.color.colorPrimaryLight));
        }
        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.drawable.filter_48));
        if (Build.VERSION.SDK_INT > 21) {
            tabLayout.getTabAt(2).getIcon().setTint(getResources().getColor(R.color.colorPrimaryLight));
        }
        tabLayout.getTabAt(3).setIcon(getResources().getDrawable(R.drawable.paraguya));
        if (Build.VERSION.SDK_INT > 21) {
            tabLayout.getTabAt(3).getIcon().setTint(getResources().getColor(R.color.colorPrimaryLight));
        }
    }



    //agregar FAB
    public void agregarFAB() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Main2Activity.this, AgregarActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }

    public void setupNavigationDrawer() {

        // Setup Navigation drawer
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        // Setup Drawer Icon
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(drawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerToggle.syncState();



        TextView usuario = (TextView) findViewById(R.id.textViewName);
        System.out.println("el usuario esta logueado??    " + User.getLogueado());
        if(User.getLogueado()){
            usuario.setText(User.getName());
            //imagen de perfil
            ImageView imageViewPictureMain = (ImageView) findViewById(R.id.imageViewPictureMain);
            Picasso.with(getApplicationContext()).load(User.getPhoto()).transform(new CircleTransform()).into(imageViewPictureMain);

        }else {
            String htmlString="<u>Iniciar Sesion</u>";
            usuario.setText(Html.fromHtml(htmlString));
            usuario.setTextColor(getResources().getColor(R.color.colorlink));

        }

        usuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(User.getLogueado()){

                }else{
                    irLogin();
                }
            }
        });

        // Hide Settings and Feedback buttons when navigation drawer is scrolled
        hideNavigationDrawerSettingsAndFeedbackOnScroll();

        // Setup RecyclerViews inside drawer
        setupNavigationDrawerRecyclerViews();
    }


    private void hideNavigationDrawerSettingsAndFeedbackOnScroll() {

        scrollViewNavigationDrawerContent = (ScrollView) findViewById(R.id.scrollViewNavigationDrawerContent);
        relativeLayoutScrollViewChild = (RelativeLayout) findViewById(R.id.relativeLayoutScrollViewChild);
        viewTreeObserverNavigationDrawerScrollView = relativeLayoutScrollViewChild.getViewTreeObserver();

        if (viewTreeObserverNavigationDrawerScrollView.isAlive()) {
            viewTreeObserverNavigationDrawerScrollView.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    if (Build.VERSION.SDK_INT > 16) {
                        relativeLayoutScrollViewChild.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        relativeLayoutScrollViewChild.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }

                    drawerHeight = relativeLayoutScrollViewChild.getHeight();
                    scrollViewHeight = scrollViewNavigationDrawerContent.getHeight();


                }
            });
        }

        onScrollChangedListener = new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {

                scrollViewNavigationDrawerContent.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {


                        return false;
                    }
                });


            }
        };

        scrollViewNavigationDrawerContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ViewTreeObserver observer;
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        observer = scrollViewNavigationDrawerContent.getViewTreeObserver();
                        observer.addOnScrollChangedListener(onScrollChangedListener);
                        break;

                    case MotionEvent.ACTION_UP:
                        observer = scrollViewNavigationDrawerContent.getViewTreeObserver();
                        observer.addOnScrollChangedListener(onScrollChangedListener);
                        break;
                }

                return false;
            }
        });
    }


    private void setupNavigationDrawerRecyclerViews() {

        // RecyclerView 1
        recyclerViewDrawer1 = (RecyclerView) findViewById(R.id.recyclerViewDrawer1);
        linearLayoutManager = new LinearLayoutManager(Main2Activity.this);
        recyclerViewDrawer1.setLayoutManager(linearLayoutManager);

        drawerItems1 = new ArrayList<>();
        drawerItems1.add(new DrawerItem(getResources().getDrawable(R.drawable.marker_48), "Cercanos"));
        drawerItems1.add(new DrawerItem(getResources().getDrawable(R.drawable.map_marker_48), "Ciudades"));
        drawerItems1.add(new DrawerItem(getResources().getDrawable(R.drawable.filter_48), "Filtrar"));
        drawerItems1.add(new DrawerItem(getResources().getDrawable(R.drawable.paraguya), "Mapa"));
        drawerItems1.add(new DrawerItem(getResources().getDrawable(R.drawable.calendar_48), "Eventos"));

        drawerAdapter1 = new DrawerAdapter(drawerItems1);
        recyclerViewDrawer1.setAdapter(drawerAdapter1);

        recyclerViewDrawer1.setMinimumHeight(convertDpToPx(144));
        recyclerViewDrawer1.setHasFixedSize(true);

        // RecyclerView Settings
        recyclerViewDrawerSettings = (RecyclerView) findViewById(R.id.recyclerViewDrawerSettings);
        linearLayoutManagerSettings = new LinearLayoutManager(Main2Activity.this);
        recyclerViewDrawerSettings.setLayoutManager(linearLayoutManagerSettings);

        drawerItemsSettings = new ArrayList<>();
        if(User.getLogueado()){
            drawerItemsSettings.add(new DrawerItem(getResources().getDrawable(R.drawable.login_rounded_48), "Cerrar Session"));
        }else{
            drawerItemsSettings.add(new DrawerItem(getResources().getDrawable(R.drawable.login_rounded_48), "Iniciar Sesion"));
        }
        drawerItemsSettings.add(new DrawerItem(getResources().getDrawable(R.drawable.facebook_64), "Facebook"));
        drawerItemsSettings.add(new DrawerItem(getResources().getDrawable(R.drawable.message_48), "Contactanos"));
        drawerItemsSettings.add(new DrawerItem(getResources().getDrawable(R.drawable.help_48), "Sobre Nosotros"));

        drawerAdapterSettings = new DrawerAdapter(drawerItemsSettings);
        recyclerViewDrawerSettings.setAdapter(drawerAdapterSettings);

        recyclerViewDrawerSettings.setMinimumHeight(convertDpToPx(96));
        recyclerViewDrawerSettings.setHasFixedSize(true);

        //colores
        colorPrimary = getResources().getColor(R.color.colorPrimaryDark);
        textColorPrimary = getResources().getColor(R.color.primary_text);
        colorControlHighlight = getResources().getColor(R.color.colorPrimaryLight);

        // Set icons alpha at start
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after some time
                for (int i = 0; i < recyclerViewDrawer1.getChildCount(); i++) {
                    ImageView imageViewDrawerItemIcon = (ImageView) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.imageViewDrawerItemIcon);
                    TextView textViewDrawerItemTitle = (TextView) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.textViewDrawerItemTitle);
                    LinearLayout linearLayoutItem = (LinearLayout) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.linearLayoutItem);
                    if (i == 0) {
                        imageViewDrawerItemIcon.setColorFilter(colorPrimary);
                        textViewDrawerItemTitle.setTextColor(colorPrimary);
                        linearLayoutItem.setBackgroundColor(colorControlHighlight);
                    } else {
                        imageViewDrawerItemIcon.setColorFilter(textColorPrimary);
                        textViewDrawerItemTitle.setTextColor(textColorPrimary);
                        linearLayoutItem.setBackgroundColor(colorBackground);
                    }
                }


                /*for (int i = 0; i < recyclerViewDrawerSettings.getChildCount(); i++) {
                    ImageView imageViewDrawerItemIcon = (ImageView) recyclerViewDrawerSettings.getChildAt(i).findViewById(R.id.imageViewDrawerItemIcon);
                    TextView textViewDrawerItemTitle = (TextView) recyclerViewDrawerSettings.getChildAt(i).findViewById(R.id.textViewDrawerItemTitle);
                    LinearLayout linearLayoutItem = (LinearLayout) recyclerViewDrawerSettings.getChildAt(i).findViewById(R.id.linearLayoutItem);
                    imageViewDrawerItemIcon.setColorFilter(textColorPrimary);
                    textViewDrawerItemTitle.setTextColor(textColorPrimary);
                    linearLayoutItem.setBackgroundColor(colorBackground);;
                }*/
            }
        }, 250);

        itemClickSupport1 = ItemClickSupport.addTo(recyclerViewDrawer1);
        itemClickSupport1.setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView parent, View view, int position, long id) {
                for (int i = 0; i < recyclerViewDrawer1.getChildCount(); i++) {
                    ImageView imageViewDrawerItemIcon = (ImageView) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.imageViewDrawerItemIcon);
                    TextView textViewDrawerItemTitle = (TextView) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.textViewDrawerItemTitle);
                    LinearLayout linearLayoutItem = (LinearLayout) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.linearLayoutItem);
                    System.out.println("posicion : " + position);
                    if (i == position) {
                        if(i == 4){
                            Intent intent = new Intent(Main2Activity.this, EventosActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            imageViewDrawerItemIcon.setColorFilter(colorPrimary);
                            textViewDrawerItemTitle.setTextColor(colorPrimary);
                            linearLayoutItem.setBackgroundColor(colorControlHighlight);
                            //cerrar el menu
                            drawerLayout.closeDrawer(GravityCompat.START);
                            //cambiar el tab layout
                            viewPager.setCurrentItem(i);
                        }

                    } else {
                        imageViewDrawerItemIcon.setColorFilter(textColorPrimary);
                        textViewDrawerItemTitle.setTextColor(textColorPrimary);
                        linearLayoutItem.setBackgroundColor(colorBackground);
                    }
                }



            }
        });

        //setings click listener
        itemClickSupport3 = ItemClickSupport.addTo(recyclerViewDrawerSettings);
        itemClickSupport3.setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        irLogin();
                        break;
                    case 1:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        irFacebook();
                        break;
                    case 2:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        Intent intent = new Intent(Main2Activity.this, InfoActivity.class);
                        intent.putExtra("fragment", "contacto");
                        startActivity(intent);
                        finish();
                        break;
                    case 3:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        boolean wrapInScrollView = true;
                        new MaterialDialog.Builder(Main2Activity.this)
                                .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                                .customView(R.layout.layout_acercade, wrapInScrollView)
                                .positiveText("OK")
                                .show();
                        break;


                }

                //despintar los demas

            }
        });

    }

    public void irLogin(){

        /*new MaterialDialog.Builder(this)
                .title("Desea cerrar Sesion?")
                .content(R.string.dialog_logout)
                .positiveText(R.string.si)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        LoginManager login = new LoginManager();
                        boolean b = false;
                        try {
                            b = login.logOut(CredentialValues.getAccessTokenData());
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if (b){
                            PrestamoRepo.vaciarPrestamos();
                            Intent intent = new Intent(HomeActivity.this, InicioActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            Snackbar.make(homeView , R.string.errorDesloguear, Snackbar.LENGTH_LONG).show();
                        }
                    }
                })
                .negativeText(R.string.no)
                .show();*/

        Intent mainIntent = new Intent(this, LoginActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainIntent);
        finish();
    }

    public void limpiarDrawer1(){
        for (int i = 0; i < recyclerViewDrawer1.getChildCount(); i++) {
            ImageView imageViewDrawerItemIcon = (ImageView) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.imageViewDrawerItemIcon);
            TextView textViewDrawerItemTitle = (TextView) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.textViewDrawerItemTitle);
            LinearLayout linearLayoutItem = (LinearLayout) recyclerViewDrawer1.getChildAt(i).findViewById(R.id.linearLayoutItem);

            imageViewDrawerItemIcon.setColorFilter(textColorPrimary);
            textViewDrawerItemTitle.setTextColor(textColorPrimary);
            linearLayoutItem.setBackgroundColor(colorBackground);

        }
    }

    public int convertDpToPx(int dp) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (dp * scale + 0.5f);
    }

    //pulsar dos veces para cerrar app
    @Override
    public void onBackPressed(){
        if (searchView.isSearchOpen() ) {
            searchView.closeSearch();
        }else if( ReciclerViewPresenter.search){
            ReciclerViewPresenter.search = false;

            Intent mainIntent = new Intent(this, MainActivity.class);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mainIntent);
            finish();

        } else{

            if(viewPager.getCurrentItem()==0){

                if (tiempoPrimerClick + INTERVALO > System.currentTimeMillis()){
                    super.onBackPressed();
                    finish();
                    return;
                }else {
                    Toast.makeText(this, "Vuelva a presionar para cerrar", Toast.LENGTH_SHORT).show();
                }
                tiempoPrimerClick = System.currentTimeMillis();
            }else  if(viewPager.getCurrentItem()==1){
                if(Utilities.back){
                    Utilities.back = false;
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    Fragment cercanos = new CiudadesFragment();
                    trans.replace(R.id.root_frame, cercanos);
                    trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    trans.addToBackStack(null);
                    trans.commit();
                }else{
                    viewPager.setCurrentItem(0);
                }

            }else  if(viewPager.getCurrentItem()==2){
                if(Utilities.back){
                    Utilities.back = false;
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    Fragment cercanos = new PersonalizadoFragment();
                    trans.replace(R.id.root_frame2, cercanos);
                    trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    trans.addToBackStack(null);
                    trans.commit();
                }else{
                    viewPager.setCurrentItem(0);
                }

            }else {
                if(Utilities.back){
                    Utilities.back = false;
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    Fragment allmaps = new AllMapsFragment();
                    trans.replace(R.id.root_frame3, allmaps);
                    trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    trans.addToBackStack(null);
                    trans.commit();
                }else{
                    viewPager.setCurrentItem(0);
                }
            }
        }

    }

    public void irFacebook( ) {

        try {
            getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/169405010203582"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent=  new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/jahugapy"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkJaime(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://jaimeferreira91"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/jaimeferreira91"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkMarcos(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://marcos-cespedes-8aa92319"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/marcos-cespedes-8aa92319"));
            startActivity(intent);
        }
    }
    public void irLinkEmilio(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://juan-emilio-espinola-874716125"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/juan-emilio-espinola-874716125"));
            startActivity(intent);
        }
    }

    public  void registerUser() {
        try{

            ThreadManager threadManager2 =  new ThreadManager(this.getApplicationContext(), "registerUser", new Cancha(), "", this.getCurrentFocus());
            threadManager2.execute();

        }catch (Exception e){
            e.printStackTrace();

        }
    }



}
