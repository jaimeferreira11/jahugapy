package py.com.ideasweb.jahugapy.pojo;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by jaime on 18/02/17.
 */

public class Evento {

    private int idevento;
    private String descripcion;
    private Cancha cancha;
    private Userapp userapp;
    private Timestamp fechaalta;
    private Date fechaevento;
    private Timestamp horadesde;
    private Timestamp horahasta;
    private Integer dimension;
    private Integer superficie;
    private Integer deporte;
    private Boolean invitar;
    private Integer totalinvitados;
    private Integer asistencia;
    private String comentarios;
    private Boolean estado;
    private Integer distancia;



    public Evento() {
    }

    public Integer getTotalinvitados() {
        return totalinvitados;
    }

    public void setTotalinvitados(Integer totalinvitados) {
        this.totalinvitados = totalinvitados;
    }

    public Integer getDeporte() {
        return deporte;
    }

    public void setDeporte(Integer deporte) {
        this.deporte = deporte;
    }

    public Evento(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getDistancia() {
        return distancia;
    }

    public void setDistancia(Integer distancia) {
        this.distancia = distancia;
    }

    public Integer getIdevento() {
        return idevento;
    }

    public void setIdevento(Integer idevento) {
        this.idevento = idevento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Cancha getCancha() {
        return cancha;
    }

    public void setCancha(Cancha cancha) {
        this.cancha = cancha;
    }

    public Userapp getUserapp() {
        return userapp;
    }

    public void setUserapp(Userapp userapp) {
        this.userapp = userapp;
    }

    public Timestamp getFechaalta() {
        return fechaalta;
    }

    public void setFechaalta(Timestamp fechaalta) {
        this.fechaalta = fechaalta;
    }

    public Date getFechaevento() {
        return fechaevento;
    }

    public void setFechaevento(Date fechaevento) {
        this.fechaevento = fechaevento;
    }

    public Timestamp getHoradesde() {
        return horadesde;
    }

    public void setHoradesde(Timestamp horadesde) {
        this.horadesde = horadesde;
    }

    public Timestamp getHorahasta() {
        return horahasta;
    }

    public void setHorahasta(Timestamp horahasta) {
        this.horahasta = horahasta;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getSuperficie() {
        return superficie;
    }

    public void setSuperficie(int superficie) {
        this.superficie = superficie;
    }

    public Boolean getInvitar() {
        return invitar;
    }

    public void setInvitar(Boolean invitar) {
        this.invitar = invitar;
    }


    public int getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(int asistencia) {
        this.asistencia = asistencia;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
