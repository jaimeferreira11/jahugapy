package py.com.ideasweb.jahugapy.ui.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import py.com.ideasweb.jahugapy.Adapter.CanchaAdaptador;
import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.ui.activities.MainActivity;
import py.com.ideasweb.jahugapy.ui.activities.SplashScreen;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalizadoFragment extends Fragment {

    MaterialBetterSpinner busqueda1;
    MaterialBetterSpinner busqueda2;
    MaterialBetterSpinner busqueda3;
    MaterialBetterSpinner param1;
    MaterialBetterSpinner param2;
    MaterialBetterSpinner param3;
    ImageView add1;
    ImageView add2;
    //valores de los spinners
    String valor1="";
    String valor2="";
    String valor3="";
    //boton
    Button buscar;

    RelativeLayout layoutbusqueda2;
    RelativeLayout layoutbusqueda3;

    String[] PARAMETROS = {"DIMENSION", "SUPERFICIE", "DEPORTE"};
    String[] DIMENSIONES = {"5 vs 5", "6 vs 6", "7 vs 7", "11 vs 11", "2 vs 2"};
    String[] SUPERFICIE = {"SINTETICO", "PISO", "NATURAL", "PARQUET"};
    String[] DEPORTETIPO = {"FUTBOL", "PADEL", "VOLLEY", "STREET SOCCER", "PIKI VOLLEY"};



    public PersonalizadoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_personalizado, container, false);



        //adaptador de parametros
        ArrayAdapter<String> paramAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, PARAMETROS);
        ArrayAdapter<String> paramAdapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, PARAMETROS);
        ArrayAdapter<String> paramAdapter2 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, PARAMETROS);

        busqueda1 = (MaterialBetterSpinner) view.findViewById(R.id.spinB1);
        busqueda2 = (MaterialBetterSpinner) view.findViewById(R.id.spinB2);
        busqueda3 = (MaterialBetterSpinner) view.findViewById(R.id.spinB3);

        param1 = (MaterialBetterSpinner) view.findViewById(R.id.spinParamB1);
        param2 = (MaterialBetterSpinner) view.findViewById(R.id.spinParamB2);
        param3 = (MaterialBetterSpinner) view.findViewById(R.id.spinParamB3);

        busqueda1.setAdapter(paramAdapter);
        busqueda2.setAdapter(paramAdapter1);
        busqueda3.setAdapter(paramAdapter2);

        layoutbusqueda2 = (RelativeLayout) view.findViewById(R.id.busqueda2);
        layoutbusqueda3 = (RelativeLayout) view.findViewById(R.id.busqueda3);

        buscar = (Button) view.findViewById(R.id.btnBuscar);

        add1 = (ImageView) view.findViewById(R.id.btnadd1);
        add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutbusqueda2.setVisibility(View.VISIBLE);
            }
        });
        add2 = (ImageView) view.findViewById(R.id.btnadd2);
        add2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutbusqueda3.setVisibility(View.VISIBLE);
            }
        });

        busqueda1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("onItemClick: " +parent.getItemAtPosition(position).toString() );
                valor1 = parent.getItemAtPosition(position).toString();
                param1.setAdapter(null);
                param1.setText("");
                ArrayAdapter<String> adapter ;
                if(valor1.equals("SUPERFICIE")){
                    adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, SUPERFICIE);
                }else if(valor1.equals("DIMENSION")){
                    adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, DIMENSIONES);
                }else{
                    adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, DEPORTETIPO);
                }
                param1.setAdapter(adapter);
                param1.setFocusable(true);

            }
        });

        busqueda2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("onItemClick: " +parent.getItemAtPosition(position).toString() );
                valor2 = parent.getItemAtPosition(position).toString();
                param2.setAdapter(null);
                param2.setText("");
                ArrayAdapter<String> adapter ;
                if(valor2.equals("SUPERFICIE")){
                    adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, SUPERFICIE);
                }else if(valor2.equals("DIMENSION")){
                    adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, DIMENSIONES);
                }else{
                    adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, DEPORTETIPO);
                }
                param2.setAdapter(adapter);
                param2.setFocusable(true);

            }
        });

        busqueda3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("onItemClick: " +parent.getItemAtPosition(position).toString() );
                valor3 = parent.getItemAtPosition(position).toString();
                param3.setAdapter(null);
                param3.setText("");
                ArrayAdapter<String> adapter ;
                if(valor3.equals("SUPERFICIE")){
                    adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, SUPERFICIE);
                }else if(valor3.equals("DIMENSION")){
                    adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, DIMENSIONES);
                }else{
                    adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, DEPORTETIPO);
                }
                param3.setAdapter(adapter);
                param3.setFocusable(true);

            }
        });

        param1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                buscar.setEnabled(true);
            }
        });

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String json = "{";
                json +=  "\""+ valor1+ "\" : \"" + param1.getText().toString()+"\"";
                if(!valor2.equals("")){
                    json +=  ",\""+ valor2+ "\" : \"" + param2.getText().toString()+"\"";
                }
                if(!valor3.equals("")){
                    json +=  ",\""+ valor3+ "\" : \"" + param3.getText().toString()+"\"";
                }
                json +=  ",\"latitud\": " +MiUbicacion.getLatitud()+",";
                json +=  "\"longitud\": " +MiUbicacion.getLongitud();
                json += "}";

                System.out.println(json );



                Bundle args = new Bundle();
                args.putString("json", json);
                if(!valor1.equals("")){
                    args.putString("valor1", valor1);
                    args.putString("param1", param1.getText().toString());
                }else{
                    args.putString("valor1", "");
                    args.putString("param1", "");
                }
                if(!valor2.equals("")){
                    args.putString("valor2", valor2);
                    args.putString("param2", param2.getText().toString());
                }else{
                    args.putString("valor2", "");
                    args.putString("param2","");
                }
                if(!valor3.equals("")){
                    args.putString("valor3", valor3);
                    args.putString("param3", param3.getText().toString());
                }else{
                    args.putString("valor3", "");
                    args.putString("param3", "");
                }
                Utilities.back = true;

                FragmentManager manager = ((AppCompatActivity)getContext()).getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                Fragment cercanos = new CanchasFiltroFragment();
                cercanos.setArguments(args);
                trans.replace(R.id.root_frame2, cercanos);
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack(null);
                trans.commit();



            }
        });

        return view;
    }


}
