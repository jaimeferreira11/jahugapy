package py.com.ideasweb.jahugapy.utilities;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import py.com.ideasweb.jahugapy.api.manager.ThreadManager;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.Evento;
import py.com.ideasweb.jahugapy.pojo.User;
import py.com.ideasweb.jahugapy.ui.activities.Main2Activity;
import py.com.ideasweb.jahugapy.ui.activities.SplashScreen;

/**
 * Created by jaime on 05/12/16.
 */

public class Utilities {
    public static Boolean back = false;
    public static Integer version_code = 0;
    public static int getDistance(Double lat_a,Double lng_a, Double lat_b, Double lon_b){
        Log.i("lat",String.valueOf(lat_a));
        int Radius = 6371000; //Radio de la tierra
       // double lat1 = lat_a / 1E6;
      //  double lat2 = lat_b / 1E6;
      //  double lon1 = lng_a / 1E6;
     //   double lon2 = lon_b / 1E6;
        double dLat = Math.toRadians(lat_b-lat_a);
        double dLon = Math.toRadians(lon_b-lng_a);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) * Math.sin(dLon /2) * Math.sin(dLon/2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return (int) (Radius * c);

    }

    public static String formatDistance(int distance){
        if(distance >= 1000){
            int k = distance / 1000;
            int m = distance - (k*1000);
            m = m / 100;
            return String.valueOf(k) + (m>0?("."+String.valueOf(m)):"") + " Km";
        } else {
            return String.valueOf(distance) + " m"+(distance==1?"":"s");
        }
    }

    //ordenar Arraylist
    public static List<Cancha> ordenarLista(List<Cancha> canchas){

        Collections.sort(canchas, new Comparator<Cancha>(){

            @Override
            public int compare(Cancha o1, Cancha o2) {
                return o1.getDistancia().compareTo(o2.getDistancia());
            }


        });

        return canchas;
    }

    //ordenar Arraylist
    public static List<Evento> ordenarEventos(List<Evento> canchas){

        Collections.sort(canchas, new Comparator<Evento>(){

            @Override
            public int compare(Evento o1, Evento o2) {
                return o1.getDistancia().compareTo(o2.getDistancia());
            }


        });

        return canchas;
    }

    public Double getRedondeo(Double valor) {
        BigDecimal bd = new BigDecimal(valor);
        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

    public String toDouble(Double value ) {
        return new DecimalFormat("#").format(value);
    }


    public static String toStringFromDate(Date date ){
        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
        return sd.format(date);
    }

    public static String toStringFromDoubleWithFormat(Double value ){
        String pattern = "###,###";
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        symbols.setGroupingSeparator('.');
        DecimalFormat df = new DecimalFormat(pattern, symbols);
        return  df.format(value);
    }

    public static boolean isNetworkConnected(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context
                .CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if (info == null || !info.isConnected() || !info.isAvailable()) {
            return false;
        }
        return true;
    }


    public static String getDeviceID(Context context){
        String deviceID = " ";
        try{
            deviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }catch (Exception ex){
            Log.e("getDeviceID", ex.getMessage());
        } return deviceID;
    }

    public static void registerUser(View v) {
        try{

            ThreadManager threadManager2 =  new ThreadManager(v.getContext(), "registerUser", new Cancha(), "", v);
            threadManager2.execute();

        }catch (Exception e){
            e.printStackTrace();

        }
    }


}
