package py.com.ideasweb.jahugapy.api.manager;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import py.com.ideasweb.jahugapy.config.Config;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.Ciudad;
import py.com.ideasweb.jahugapy.pojo.Evento;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.pojo.Respuesta;
import py.com.ideasweb.jahugapy.pojo.User;
import py.com.ideasweb.jahugapy.utilities.JsonHelper;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * Created by jaime on 05/12/16.
 */

//clase que obtendra los datos por Rest-Api
public class Manager {

    public ArrayList<Cancha> canchasrepo(){

        Log.i("latitud", String.valueOf(MiUbicacion.getLatitud()));
        Log.i("longitud", String.valueOf(MiUbicacion.getLongitud()));

         ArrayList<Cancha> canchas = new ArrayList<>();

       /*canchas.add(new Cancha(1, "Papagol", "254545", "Madam Lynch c/ Mcal Lopez","09875326", "-25.30952983340003", "-57.55325870063939"));
        canchas.add(new Cancha(2, "Arrayanes", "5255555", "Mcal Lopez 1455","09875326","-25.304660842237936", "-57.556359334260605"));*/


        return canchas;
    }

    public Respuesta getCanchas() throws Exception {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Respuesta respuesta = new Respuesta();

        String latitud = String.valueOf(MiUbicacion.getLatitud().toString());
        String longitud = String.valueOf(MiUbicacion.getLongitud().toString());

        System.out.println("la url de canchas es : " + Config.API_GET_CANCHAS_BY_LOCALIZATION+latitud+"/"+longitud+"/");

        URL url = new URL(Config.API_GET_CANCHAS_BY_LOCALIZATION + latitud+"/"+longitud+"/");

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setRequestMethod("GET");
        urlConnection.setConnectTimeout(10000); //5 segundos

        //urlConnection.setRequestProperty("Authorization", "Bearer " + token.getAccess_token());

        int code = urlConnection.getResponseCode();

        BufferedInputStream in;

        System.out.println("La respuesta de getcanchas es: " + code);

        if (code >= 400) {

            in = new BufferedInputStream(urlConnection.getErrorStream());
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            respuesta.setEstado("ERROR");
            respuesta.setError("El servicio no esta disponible");

        } else {

            in = new BufferedInputStream(urlConnection.getInputStream());
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";

            respuesta = JsonHelper.fromJson(result, Respuesta.class);

            System.out.println("El estado de la respuesta: " + respuesta.getEstado());

            System.out.println("El object es: " + respuesta.getDatos().toString());

            Gson gson = new Gson();

            String jsonInString = gson.toJson(respuesta.getDatos());

            System.out.println("El dato desserializado es: " + jsonInString);

            Type listType = new TypeToken<List<Cancha>>() {}.getType();
            List<Cancha> listCanchas = new Gson().fromJson(jsonInString, listType);
            respuesta.setDatos(listCanchas);

            System.out.println("la lista de canchas es: " + listCanchas.size());




        }

        return respuesta;

    }


    public Respuesta getCanchasByCiudad(String idciudad) throws Exception {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Respuesta respuesta = new Respuesta();
        String latitud = String.valueOf(MiUbicacion.getLatitud().toString());
        String longitud = String.valueOf(MiUbicacion.getLongitud().toString());

        if(!idciudad.equals("")){

            System.out.println("la url de canchas by idciudad es : " + Config.API_GET_CANCHAS_BY_CIUDAD+idciudad+"/"+latitud+"/"+longitud+"/");

            URL url = new URL(Config.API_GET_CANCHAS_BY_CIUDAD+idciudad+"/"+latitud+"/"+longitud+"/");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");


            int code = urlConnection.getResponseCode();
            BufferedInputStream in;
            System.out.println("La respuesta de getcanchas by idciudad "+idciudad +": " + code);

            if (code >= 400) {

                in = new BufferedInputStream(urlConnection.getErrorStream());
                Scanner s = new Scanner(in).useDelimiter("\\A");
                String result = s.hasNext() ? s.next() : "";
                respuesta.setEstado("ERROR");
                respuesta.setError(result);

            } else {

                in = new BufferedInputStream(urlConnection.getInputStream());
                Scanner s = new Scanner(in).useDelimiter("\\A");
                String result = s.hasNext() ? s.next() : "";

                respuesta = JsonHelper.fromJson(result, Respuesta.class);

                System.out.println("El estado de la respuesta: " + respuesta.getEstado());

                System.out.println("El object es: " + respuesta.getDatos().toString());

                Gson gson = new Gson();

                String jsonInString = gson.toJson(respuesta.getDatos());

                System.out.println("El dato desserializado es: " + jsonInString);

                Type listType = new TypeToken<List<Cancha>>() {}.getType();
                List<Cancha> listCanchas = new Gson().fromJson(jsonInString, listType);
                respuesta.setDatos(listCanchas);

                System.out.println("la lista de canchas es: " + listCanchas.size());


            }
        }

        return respuesta;

    }

    //traer las ciudades del web services

    public  Respuesta getCiudades() throws Exception {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Respuesta respuesta = new Respuesta();

        URL url = new URL(Config.API_GET_CIUDADES);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");

        int code = urlConnection.getResponseCode();
        BufferedInputStream in;
        System.out.println("La respuesta de getciudades es: " + code);

        if (code >= 400) {
            in = new BufferedInputStream(urlConnection.getErrorStream());
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            respuesta.setEstado("ERROR");
            respuesta.setError(result);
        } else {



            in = new BufferedInputStream(urlConnection.getInputStream());
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";

            respuesta = JsonHelper.fromJson(result, Respuesta.class);

            System.out.println("El estado de la respuesta: " + respuesta.getEstado());

            System.out.println("El object es: " + respuesta.getDatos().toString());

            Gson gson = new Gson();

            String jsonInString = gson.toJson(respuesta.getDatos());

            System.out.println("El dato desserializado es: " + jsonInString);

            Type listType = new TypeToken<List<Ciudad>>() {}.getType();
            List<Ciudad> listCanchas = new Gson().fromJson(jsonInString, listType);
            respuesta.setDatos(listCanchas);



        }
        return respuesta;
    }

    //add cancha
    public Respuesta insertCancha(Cancha cancha) throws  Exception{

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        Respuesta respuesta = new Respuesta();
        respuesta.setDatos("");
        respuesta.setEstado("OK");

        String datos = JsonHelper.toJson(cancha, Cancha.class);

        System.out.println("enviando datos " +  datos);

        URL url = new URL(Config.API_POS_INSERT_CANCHA);

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setRequestMethod( "POST" );
        urlConnection.setConnectTimeout(10000); //5 segundos


        String urlParameters = "cancha="+datos+"&user="+ User.getEmail();

        System.out.println("la url es: " + Config.API_POS_INSERT_CANCHA);


        urlConnection.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int code = urlConnection.getResponseCode();

        BufferedInputStream in;

        if (code >= 400 ) {

            in = new BufferedInputStream( urlConnection.getErrorStream() );
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            System.out.println("El error es: " + result);

            //respuesta = JsonHelper.fromJson(result, Respuesta.class);
            respuesta.setEstado("ERROR");
            respuesta.setError(result);

        } else {

            in = new BufferedInputStream( urlConnection.getInputStream() );
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";

            respuesta = JsonHelper.fromJson(result, Respuesta.class);

            System.out.println("La respuesta es: " + respuesta.getEstado());

        }

        return respuesta;

    }


    public Respuesta registerUser(Context context) throws  Exception{

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Respuesta respuesta = new Respuesta();
        respuesta.setDatos("");
        respuesta.setEstado("OK");

        if(User.getEmail() != null) {
            if (User.getEmail().equals("")) {
                User.setEmail("SIN_EMAIL");
            }
        }else{
            User.setEmail("SIN_EMAIL");
        }
        if(User.getIddevice() == null || User.getIddevice().equals("")){
           User.setIddevice("sin id");
        }
        System.out.println("El ID  es: " + User.getIddevice());
        System.out.println("El email  es: " + User.getEmail());
        System.out.println("la url es: " + Config.API_POST_USER_REGISTER);

        URL url = new URL(Config.API_POST_USER_REGISTER);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        Map<String, Object> map = new HashMap<>();
        map.put("iddevice", User.getIddevice());
        map.put("email", User.getEmail());
        map.put("latitud", User.getLatitud());
        map.put("longitud", User.getLongitud());
        map.put("name", User.getName());
        map.put("googleid", User.getGoogleid());
        map.put("logueado", User.getLogueado());


        String json = "{";
        json +=  "\"iddevice\": \"" +User.getIddevice()+"\",";
        json +=  "\"email\": \"" +User.getEmail()+"\",";
//        json +=  "\"photo\": \"" +User.getPhoto()+"\",";
        json +=  "\"latitud\": " +MiUbicacion.getLatitud()+",";
        json +=  "\"longitud\": " +MiUbicacion.getLatitud()+",";
        json +=  "\"name\": \"" +User.getName()+"\",";
        json +=  "\"googleid\": \"" +User.getGoogleid()+"\",";
        json +=  "\"logueado\": " +User.getLogueado();
        json += "}";



        urlConnection.setRequestMethod( "POST" );
        urlConnection.setConnectTimeout(10000); //5 segundos
        String urlParameters = "user="+ json;

        urlConnection.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();



            int code = urlConnection.getResponseCode();

            BufferedInputStream in;

            if (code >= 400 ) {

                in = new BufferedInputStream( urlConnection.getErrorStream() );
                Scanner s = new Scanner(in).useDelimiter("\\A");
                String result = s.hasNext() ? s.next() : "";
                System.out.println("El error es: " + result);
                respuesta.setEstado("ERROR");
                respuesta.setError(result);


            } else {

                in = new BufferedInputStream( urlConnection.getInputStream() );
                Scanner s = new Scanner(in).useDelimiter("\\A");
                String result = s.hasNext() ? s.next() : "";

                respuesta = JsonHelper.fromJson(result, Respuesta.class);
                System.out.println("La respuesta de registar usuario es: " + respuesta.getDatos());

                String res = (String) respuesta.getDatos();
                String[] parts = res.split("-");
                Utilities.version_code = Integer.parseInt(parts[0].replace(".0", ""));
                User.setIduser(Integer.parseInt(parts[1]));
            }

            urlConnection.disconnect();
     //  }




        return respuesta;
    }


    public Respuesta getCanchasByParam(Cancha cancha) throws Exception {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Respuesta respuesta = new Respuesta();
        String latitud = String.valueOf(MiUbicacion.getLatitud().toString());
        String longitud = String.valueOf(MiUbicacion.getLongitud().toString());


        System.out.println("la url de getCanchasByParam es : " + Config.API_GET_CANCHAS_BY_PARAM+cancha.getDenominacion().replace(" ", "%")+"/"+latitud+"/"+longitud+"/");

        URL url = new URL(Config.API_GET_CANCHAS_BY_PARAM+cancha.getDenominacion().replace(" ", "_")+"/"+latitud+"/"+longitud+"/");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");


        int code = urlConnection.getResponseCode();
        BufferedInputStream in;
        System.out.println("La respuesta de la busqueda para  "+cancha.getDenominacion() +": " + code);

        if (code >= 400) {

            in = new BufferedInputStream(urlConnection.getErrorStream());
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            respuesta.setEstado("ERROR");
            respuesta.setError(result);

        } else {

            in = new BufferedInputStream(urlConnection.getInputStream());
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";

            respuesta = JsonHelper.fromJson(result, Respuesta.class);

            System.out.println("El estado de la respuesta: " + respuesta.getEstado());

            System.out.println("El object es: " + respuesta.getDatos().toString());

            Gson gson = new Gson();

            String jsonInString = gson.toJson(respuesta.getDatos());

            System.out.println("El dato desserializado es: " + jsonInString);

            Type listType = new TypeToken<List<Cancha>>() {}.getType();
            List<Cancha> listCanchas = new Gson().fromJson(jsonInString, listType);
            respuesta.setDatos(listCanchas);

            System.out.println("la lista de canchas para "+cancha.getDenominacion()+" es: " + listCanchas.size());


        }

        return respuesta;

    }


    public Respuesta getCanchasByFiltro(String json) throws Exception {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Respuesta respuesta = new Respuesta();
        String latitud = String.valueOf(MiUbicacion.getLatitud().toString());
        String longitud = String.valueOf(MiUbicacion.getLongitud().toString());



        URL url = new URL(Config.API_GET_CANCHAS_BY_FILTRO);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");

        String urlParameters = "json="+json;

        System.out.println("la url de canchas by filtro es : " + Config.API_GET_CANCHAS_BY_FILTRO+ urlParameters);

        urlConnection.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();


        int code = urlConnection.getResponseCode();
        BufferedInputStream in;
        System.out.println("La respuesta de la busqueda para  : " + code);

        if (code >= 400) {

            in = new BufferedInputStream(urlConnection.getErrorStream());
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            respuesta.setEstado("ERROR");
            respuesta.setError(result);

        } else {

            in = new BufferedInputStream(urlConnection.getInputStream());
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";

            respuesta = JsonHelper.fromJson(result, Respuesta.class);

            System.out.println("El estado de la respuesta: " + respuesta.getEstado());

            System.out.println("El object es: " + respuesta.getDatos().toString());

            Gson gson = new Gson();

            String jsonInString = gson.toJson(respuesta.getDatos());

            System.out.println("El dato desserializado es: " + jsonInString);

            Type listType = new TypeToken<List<Cancha>>() {}.getType();
            List<Cancha> listCanchas = new Gson().fromJson(jsonInString, listType);
            respuesta.setDatos(listCanchas);

            System.out.println("la lista de canchas para es: " + listCanchas.size());


        }

        return respuesta;

    }

    public Respuesta getEventos() throws Exception {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Respuesta respuesta = new Respuesta();

        String latitud = String.valueOf(MiUbicacion.getLatitud().toString());
        String longitud = String.valueOf(MiUbicacion.getLongitud().toString());

        System.out.println("la url de eventos es : " + Config.API_GET_EVENTOS+latitud+"/"+longitud+"/");

        URL url = new URL(Config.API_GET_EVENTOS +"/"+ latitud+"/"+longitud+"/");

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setRequestMethod("GET");
        urlConnection.setConnectTimeout(10000); //5 segundos

        int code = urlConnection.getResponseCode();

        BufferedInputStream in;

        System.out.println("La respuesta de geteventos  es: " + code);

        if (code >= 400) {

            in = new BufferedInputStream(urlConnection.getErrorStream());
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            respuesta.setEstado("ERROR");
            respuesta.setError("El servicio no esta disponible");

        } else {

            in = new BufferedInputStream(urlConnection.getInputStream());
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";

            respuesta = JsonHelper.fromJson(result, Respuesta.class);

            System.out.println("El estado de la respuesta: " + respuesta.getEstado());

            System.out.println("El object es: " + respuesta.getDatos().toString());

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm'Z'").create();

            String jsonInString = gson.toJson(respuesta.getDatos());

            System.out.println("El dato desserializado es: " + jsonInString);

            Type listType = new TypeToken<List<Evento>>() {}.getType();
            List<Evento> listCanchas = gson.fromJson(jsonInString, listType);
            respuesta.setDatos(listCanchas);

            System.out.println("la lista de eventos es: " + listCanchas.size());




        }

        return respuesta;
    }

    public Respuesta insertEvento(Evento evento) throws Exception{

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Respuesta respuesta = new Respuesta();
        respuesta.setDatos("");
        respuesta.setEstado("OK");
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm'Z'").create();
        String datos = gson.toJson(evento, Evento.class);
        System.out.println("enviando datos " +  datos);
        URL url = new URL(Config.API_POST_EVENTO);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod( "POST" );
        urlConnection.setConnectTimeout(10000); //5 segundos

        String urlParameters = "evento="+datos;
        System.out.println("la url es: " + Config.API_POST_EVENTO);

        urlConnection.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int code = urlConnection.getResponseCode();

        BufferedInputStream in;

        if (code >= 400 ) {
            in = new BufferedInputStream( urlConnection.getErrorStream() );
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            System.out.println("El error es: " + result);
            //respuesta = JsonHelper.fromJson(result, Respuesta.class);
            respuesta.setEstado("ERROR");
            respuesta.setError(result);

        } else {

            in = new BufferedInputStream( urlConnection.getInputStream() );
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";respuesta = JsonHelper.fromJson(result, Respuesta.class);

            System.out.println("La respuesta es: " + respuesta.getEstado());

        }

        return respuesta;
    }

    // Asistencia de evento
    public Respuesta asistirEvento(String id, String ban) throws Exception{

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Respuesta respuesta = new Respuesta();
        respuesta.setDatos("");
        respuesta.setEstado("OK");



        URL url = new URL(Config.API_POST_ASISTIR_EVENTO+"/"+User.getIduser()+"/"+id+"/"+ban);
        System.out.println(Config.API_POST_ASISTIR_EVENTO+"/"+User.getIduser()+"/"+id+"/"+ban);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod( "POST" );
        urlConnection.setConnectTimeout(10000); //5 segundos

        int code = urlConnection.getResponseCode();

        BufferedInputStream in;

        if (code >= 400 ) {
            in = new BufferedInputStream( urlConnection.getErrorStream() );
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            System.out.println("El error es: " + result);
            //respuesta = JsonHelper.fromJson(result, Respuesta.class);
            respuesta.setEstado("ERROR");
            respuesta.setError(result);

        } else {

            in = new BufferedInputStream( urlConnection.getInputStream() );
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";respuesta = JsonHelper.fromJson(result, Respuesta.class);

            System.out.println("La respuesta es: " + respuesta.getEstado());

        }

        return respuesta;
    }

}