package py.com.ideasweb.jahugapy.api.manager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.EOFException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.Evento;
import py.com.ideasweb.jahugapy.pojo.Respuesta;
import py.com.ideasweb.jahugapy.ui.activities.AgregarActivity;
import py.com.ideasweb.jahugapy.ui.activities.Main2Activity;
import py.com.ideasweb.jahugapy.ui.activities.SplashScreen;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.Utilities;


/**
 * Created by jaime on 14/12/16.
 */

public class ThreadManagerEvento extends AsyncTask<Void,Void,Respuesta> {

    //Declaring Variables
    private String metodo;
    private Evento evento;
    private String msgOk;
    private View view;
    //Progressdialog to show while sending email
    private ProgressDialog progressDialog;


    //Class Constructor
    public ThreadManagerEvento(String metodo, Evento evento, String msgOk, View view){

        this.metodo = metodo;
        this.evento = evento;
        this.msgOk = msgOk;
        this.view = view;

    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        System.out.println(metodo);
       /* if (metodo.equals("getCanchas") || metodo.equals("registerUser")) {

        }else{
            progressDialog = ProgressDialog.show(context,"Enviando datos","Favor aguarde...",false,false);

        }*/
        progressDialog = ProgressDialog.show(view.getContext(),"Enviando datos","Favor aguarde...",false,false);

    }

    @Override
    protected void onPostExecute(Respuesta respuesta) {
        super.onPostExecute(respuesta);

        if (respuesta.getEstado() != null) {
            if (respuesta.getEstado().equals("OK")) {


                    new MaterialDialog.Builder(view.getContext())
                            .title(msgOk)
                            .content("Su evento aparece en la lista")
                            .titleColor(view.getContext().getResources().getColor(R.color.colorPrimary))
                            .icon(view.getContext().getResources().getDrawable(R.drawable.checked_48))
                            .positiveText("OK")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    Intent intent = new Intent(view.getContext(), Main2Activity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    view.getContext().startActivity(intent);
                                    ((AgregarActivity)view.getContext()).finish();

                                    //inicializarFormulario(view);
                                }
                            })
                            .show();
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

            } else {
                new MaterialDialog.Builder(view.getContext())
                        .title("Error")
                        .content("Intente mas tarde")
                        .titleColor(view.getContext().getResources().getColor(R.color.colorPrimary))
                         .icon(view.getContext().getResources().getDrawable(R.drawable.cancel_48))
                        .content(respuesta.getError())
                        .positiveText("OK")
                        .show();
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }

        }else  {
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            new MaterialDialog.Builder(view.getContext())
                    .title("Error")
                    .content("Intente mas tarde")
                    .titleColor(view.getContext().getResources().getColor(R.color.colorPrimary))
                    .icon(view.getContext().getResources().getDrawable(R.drawable.cancel_48))
                    .content(respuesta.getError())
                    .positiveText("OK")
                    .show();
        }
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

    }



    @Override
    protected Respuesta doInBackground(Void... params) {

        Respuesta respuesta = new Respuesta();
        try {
            Manager manager = new Manager();


            if (metodo.equals("insertEvento")) {
                respuesta = manager.insertEvento(evento);
            }


        }catch (ConnectException c){

            c.printStackTrace();
            Snackbar.make(view,"Servicio No disponible" , Snackbar.LENGTH_LONG).show();

        }catch (SocketTimeoutException e){

            e.printStackTrace();
            Snackbar.make(view,"Servicio No disponible" , Snackbar.LENGTH_LONG).show();

        }catch (EOFException e){
            e.printStackTrace();
            Snackbar.make(view,"Servicio No disponible" , Snackbar.LENGTH_LONG).show();

        }catch (Exception e){
            e.printStackTrace();
//            Toast.makeText(context, "Error " + e.getClass().getCanonicalName(), Toast.LENGTH_LONG);
        }
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        return respuesta;
    }





}
