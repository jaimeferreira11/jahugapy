package py.com.ideasweb.jahugapy.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;



public class PrefManager {
    private static final String TAG = PrefManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "jahuga";


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);

    }



    //favoritos
    public void setEvento(int id) {
        editor = pref.edit();
        String cadena = "";
        if(getEvento().equals("")){
            cadena =  String.valueOf(id);
        }else{
            String[] parts = getEvento().split("-");
            for (int i = 0; i < parts.length; i++) {
                if(parts[i] != String.valueOf(id)){
                    cadena = getEvento() + "-" + String.valueOf(id);
                }
            }
        }
        editor.putString("evento", cadena);
        System.out.println("se guardo : " + cadena);
        // commit changes
        editor.commit();
    }

    public String getEvento() {
        return pref.getString("evento", "");
    }

    public void removeFav(int id){
        String cadena = getEvento();

        String[] parts = cadena.split("-");
        System.out.println("partes: " + parts.length);
        for (int i = 0; i <  parts.length; i++) {
            parts[i] = parts[i].replace("-", "");
        }
        pref.edit().clear().commit();

        for (int i = 0; i <  parts.length; i++) {
            if(!parts[i].equals(String.valueOf(id))){
                setEvento(Integer.parseInt(parts[i]));
            }
        }
    }


}