package py.com.ideasweb.jahugapy.pojo;

/**
 * Created by jaime on 20/02/17.
 */

public class Userapp {

    private  Integer iduser;
    private  String iddevice;
    private  String email;
    private  Double latitud;
    private  Double longitud;
    private  String phone;

    private  String name;
    private  String photo;
    private  String googleid ;

    public Userapp() {
    }

    public Integer getIduser() {
        return iduser;
    }

    public void setIduser(Integer iduser) {
        this.iduser = iduser;
    }

    public String getIddevice() {
        return iddevice;
    }

    public void setIddevice(String iddevice) {
        this.iddevice = iddevice;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getGoogleid() {
        return googleid;
    }

    public void setGoogleid(String googleid) {
        this.googleid = googleid;
    }
}
