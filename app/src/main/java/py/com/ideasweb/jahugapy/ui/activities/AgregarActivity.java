package py.com.ideasweb.jahugapy.ui.activities;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Calendar;

import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.api.manager.ThreadManager;
import py.com.ideasweb.jahugapy.config.Config;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.Ciudad;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.utilities.Share;
import py.com.ideasweb.jahugapy.utilities.Utilities;
import py.com.ideasweb.jahugapy.utilities.Validation;

public class AgregarActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener{

    public static  ArrayList<Ciudad> ciudades = new ArrayList<>();

    private Toolbar mToolbar;
    TextView mTitle;
    //correo
    EditText direccion;
    EditText comentarios ;
    EditText  nombre;
    EditText cel;
    EditText lineabaja;
    EditText hora;
    EditText cantidad;
    EditText precio ;
    EditText precio1;

    //ciudad
    MaterialBetterSpinner ciudad;
    MaterialBetterSpinner diadesde;
    MaterialBetterSpinner diahasta;
    Ciudad ciudadSeleccionada;
    //dimemsion
    CheckBox d5v5;
    CheckBox d6v6;
    CheckBox d7v7;
    EditText otrodimen;
    //superficie
    CheckBox sintetico;
    CheckBox natural;
    CheckBox piso;
    EditText otrosuper;
    //deporte
    CheckBox futbol;
    CheckBox volley;
    CheckBox padel;
    EditText otrodeporte;
    //
    CheckBox checkin;

    Button aceptar;
    View currentview;



    String[] DIAS = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabados", "Domingo"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar);

        // se agrega el toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        //se centra el titulo
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        mTitle.setText(mToolbar.getTitle());
        //se oculta el title por defecto
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        currentview = getCurrentFocus();

        // Disable Scrolling by setting up an OnTouchListener to do nothing

        //lista
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, DIAS);
        inicializarCiudades();


        direccion = (EditText) findViewById(R.id.addDireccion);
        comentarios = (EditText) findViewById(R.id.addComentarios);
        nombre = (EditText) findViewById(R.id.AddNombre);
        cel =(EditText) findViewById(R.id.addCelular) ;
        lineabaja = (EditText) findViewById(R.id.addLineaBaja);
        hora = (EditText) findViewById(R.id.tvHora);
        cantidad = (EditText) findViewById(R.id.addCantidad);
        precio = (EditText) findViewById(R.id.addPrecio);
        precio1 = (EditText) findViewById(R.id.addPrecio1);
        comentarios = (EditText) findViewById(R.id.addComentarios);

        precio.addTextChangedListener(new TextWatcher() {

            boolean isManualChange = false;

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (isManualChange) {
                    isManualChange = false;
                    return;
                }

                try {
                    String value = s.toString().replace(".", "");
                    String reverseValue = new StringBuilder(value).reverse()
                            .toString();
                    StringBuilder finalValue = new StringBuilder();
                    for (int i = 1; i <= reverseValue.length(); i++) {
                        char val = reverseValue.charAt(i - 1);
                        finalValue.append(val);
                        if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                            finalValue.append(".");
                        }
                    }
                    isManualChange = true;
                    precio.setText(finalValue.reverse());
                    precio.setSelection(finalValue.length());
                } catch (Exception e) {
                    // Do nothing since not a number
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        precio1.addTextChangedListener(new TextWatcher() {

            boolean isManualChange = false;

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (isManualChange) {
                    isManualChange = false;
                    return;
                }

                try {
                    String value = s.toString().replace(".", "");
                    String reverseValue = new StringBuilder(value).reverse()
                            .toString();
                    StringBuilder finalValue = new StringBuilder();
                    for (int i = 1; i <= reverseValue.length(); i++) {
                        char val = reverseValue.charAt(i - 1);
                        finalValue.append(val);
                        if (i % 3 == 0 && i != reverseValue.length() && i > 0) {
                            finalValue.append(".");
                        }
                    }
                    isManualChange = true;
                    precio1.setText(finalValue.reverse());
                    precio1.setSelection(finalValue.length());
                } catch (Exception e) {
                    // Do nothing since not a number
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        //ciudad
        ArrayAdapter<Ciudad> ciudadList = new ArrayAdapter<Ciudad>(this,  android.R.layout.simple_dropdown_item_1line, ciudades);

        ciudad = (MaterialBetterSpinner)findViewById(R.id.addCiudad);
        ciudad.setAdapter(ciudadList);

        ciudad.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ciudadSeleccionada = (Ciudad) adapterView.getItemAtPosition(i);


            }
        });

        checkin = (CheckBox) findViewById(R.id.checkin);
        //de
        diadesde = (MaterialBetterSpinner)findViewById(R.id.addDiaDesde);
        diadesde.setAdapter(arrayAdapter);
        //hasta
        diahasta= (MaterialBetterSpinner) findViewById(R.id.addDiaHasta);
        diahasta.setAdapter(arrayAdapter);

        //dimen
        d5v5 = (CheckBox) findViewById(R.id.add5v5);
        d6v6 = (CheckBox) findViewById(R.id.add6v6);
        d7v7 = (CheckBox) findViewById(R.id.add7v7);
        otrodimen = (EditText) findViewById(R.id.addOtroDimen);
        //deporte
        futbol = (CheckBox) findViewById(R.id.addFutbol);
        volley = (CheckBox) findViewById(R.id.addVolley);
        padel = (CheckBox) findViewById(R.id.addPadel);
        otrodeporte = (EditText) findViewById(R.id.addOtroDeporte);
        //superficie
        sintetico = (CheckBox) findViewById(R.id.addSintetico);
        natural = (CheckBox) findViewById(R.id.addNatural);
        piso = (CheckBox) findViewById(R.id.addPiso);
        otrosuper = (EditText) findViewById(R.id.addOtroSuperficie);
        //boton
        aceptar = (Button)findViewById(R.id.addAceptar);





    }

    //infla en el activity el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // menu de opciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mabout:
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                        .customView(R.layout.layout_acercade, wrapInScrollView)
                        .positiveText("OK")
                        .show();
                break;
            case R.id.mshare:
                Share.shareRedeSociales(this, Config.API_DOWNLOAD_APK, "JahugaPy - Encuentra las canchas deportivas cercanas a ti!!");
                break;
            case R.id.mcontact:
                Intent intent = new Intent(this, InfoActivity.class);
                intent.putExtra("fragment", "contacto");
                startActivity(intent);
                finish();
                break;
            case android.R.id.home:
                Intent i = new Intent(AgregarActivity.this, Main2Activity.class);
                startActivity(i);
                finish();

                break;
            case R.id.mfanPage:
                irFacebook();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){

        if(keyCode == KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(AgregarActivity.this, Main2Activity.class);
            startActivity(intent);
            finish();

        }
        return super.onKeyDown(keyCode, event);
    }

    // envio de correo electronico
   /* public void enviarCorreo() {
        if (checkValidation ()) {
            sendEmail(direccion.getText().toString(), mensaje.getText().toString(), nombre.getText().toString(), cel.getText().toString());
            inicializarCampos();

        } else {
            // Toast.makeText(this, "Complete los campos..", Toast.LENGTH_SHORT).show();
            // nombre.requestFocus();
        }
    }*/


  /*  private void sendEmail(String documento, String mensaje, String nombre, String cel) {
        //Creating SendMail object
        SendMail sm = new SendMail(this, "" , "Contacto FupApp", mensaje+"\n \n"+nombre+"\nCI: "+documento+"\nCel: "+cel, getCurrentFocus());
        //Executing sendmail to send email
        sm.execute();
    }*/
  public void irFacebook( ) {

      try {
          getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
          Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/169405010203582"));
          startActivity(intent);
      } catch (Exception e) {
          Intent intent=  new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/jahugapy"));
          startActivity(intent);
      }
  }

    public void inicializarCampos() {
        nombre.setText(null);
        direccion.setText(null);
        comentarios.setText(null);
        cel.setText(null);
        // nombre.requestFocus();
    }

    // validacion de formulario
    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText((EditText) findViewById(R.id.AddNombre))) ret = false;
        if (!Validation.hasText((EditText) findViewById(R.id.addDireccion))) ret = false;
        if (!Validation.hasText((EditText) findViewById(R.id.addCelular))) ret = false;
        if (!Validation.hasText((EditText) findViewById(R.id.addCantidad))) ret = false;
        if (!Validation.hasText((EditText) findViewById(R.id.addPrecio))) ret = false;
        if (!Validation.hasText((EditText) findViewById(R.id.tvHora))) ret = false;
        if(diadesde.getText().toString().length() == 0){
            ret = false;
            diadesde.setError("Campo Requerido");
        }
        if(diahasta.getText().toString().length() == 0){
            ret = false;
            diahasta.setError("Campo Requerido");
        }
        if(ciudad.getText().toString().length() == 0){
            ret = false;
            ciudad.setError("Campo Requerido");
        }

        return ret;
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        String minuteString = minute < 10 ? "0"+minute : ""+minute;
        String hourStringEnd = hourOfDayEnd < 10 ? "0"+hourOfDayEnd : ""+hourOfDayEnd;
        String minuteStringEnd = minuteEnd < 10 ? "0"+minuteEnd : ""+minuteEnd;
        String time = hourString+":"+minuteString+" a "+hourStringEnd+":"+minuteStringEnd;


        hora.setText(time);

    }

    public void verPicker(View view){
        //time
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                AgregarActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.show(getFragmentManager(), "Hora de Atencion");
    }


   public void addEnviar(View view){
       Cancha cancha = new Cancha();
       if(Utilities.isNetworkConnected(getApplicationContext())){
          if(checkValidation()){
               cancha.setNombrefantasia(nombre.getText().toString().toUpperCase());
               cancha.setDenominacion(nombre.getText().toString().toUpperCase());
               cancha.setDireccion(direccion.getText().toString().toUpperCase());
              if(checkin.isChecked() ){
                  cancha.setLatitud(String.valueOf(MiUbicacion.getLatitud()));
                  cancha.setLongitud(String.valueOf(MiUbicacion.getLongitud()));
              }else{
                  cancha.setLatitud("0");
                  cancha.setLongitud("0");
              }
               cancha.setTelefono(lineabaja.getText().toString());
              cancha.setCelular(cel.getText().toString());
              cancha.setPreciohora(Double.parseDouble(precio.getText().toString().replace(".", "")));
              if(!precio1.getText().toString().equals("")){
                cancha.setPreciohora2(Double.parseDouble(precio1.getText().toString().replace(".", "")));
              }else{
                  cancha.setPreciohora2(new Double(0));
              }
               cancha.setCantidadcanchas(Integer.parseInt(cantidad.getText().toString()));
               Object [] obj = new Object[2];
               obj = separarHora(hora.getText().toString());

               cancha.setAtenciondesde(obj[0].toString());
               cancha.setAtencionhasta(obj[1].toString());
              //dia de atencion
              cancha.setDiadesde(diadesde.getText().toString());
              cancha.setDiahasta(diahasta.getText().toString());
              //distrito
              if(ciudadSeleccionada.getDescripcion() != null){
                  cancha.setIddistrito(ciudadSeleccionada.getIddistrito());
              }
               String dimension="";
               if(d5v5.isChecked()){
                   dimension = d5v5.getHint().toString();
               }
               if(d6v6.isChecked()){
                   dimension = dimension + "|"+d6v6.getHint().toString();
               }
              if(d7v7.isChecked()){
                  dimension = dimension + "|"+d7v7.getHint().toString();
              }
               if(otrodimen.getText().toString().length() != 0){
                   dimension = dimension + "|"+otrodimen.getText().toString().toUpperCase();
               }
               cancha.setDimenciones(dimension);
               String deporte="";
               if(futbol.isChecked()){

                   deporte = futbol.getHint().toString();
               }
               if(volley.isChecked()){
                   deporte = deporte+"|"+ volley.getHint().toString();
               }
               if(padel.isChecked()){

                   deporte = deporte + "|"+padel.getHint().toString();
               }
               if(otrodeporte.getText().toString().length() != 0){
                   deporte = deporte + "|"+otrodeporte.getText().toString().toUpperCase();
               }
               cancha.setDeportestipo(deporte);
               String superficie="";
               if(sintetico.isChecked()){
                   superficie = sintetico.getHint().toString();
               }
               if(natural.isChecked()){

                   superficie = superficie+ "|"+natural.getHint().toString();
               }
               if(piso.isChecked()){
                   superficie = superficie + "|"+piso.getHint().toString();
               }
               if(otrosuper.getText().toString().length() != 0){
                   superficie = superficie + "|"+otrosuper.getText().toString().toUpperCase();
               }
               cancha.setSuperficie(superficie);
               cancha.setComentario(comentarios.getText().toString());

               ThreadManager threadManager =  new ThreadManager(view.getContext(), "insertCancha",cancha, "Registro Agregado Correctamente", getCurrentFocus());
               threadManager.execute();
           }
       }else{
           Snackbar.make(getCurrentFocus(), "Verifique su conexion a Internet", Snackbar.LENGTH_LONG).show();
       }


   }

    public Object[] separarHora(String hora){
        Object [] obj = new Object[2];
        String[] parts = hora.split("a");
        obj[0] = parts[0].trim();
        obj[1] = parts[1].trim();

        return obj;
    }

    //Link redes sociales
    public void irLinkJaime(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://jaimeferreira91"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/jaimeferreira91"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkMarcos(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://marcos-cespedes-8aa92319"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/marcos-cespedes-8aa92319"));
            startActivity(intent);
        }
    }
    public void irLinkEmilio(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://juan-emilio-espinola-874716125"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/juan-emilio-espinola-874716125"));
            startActivity(intent);
        }
    }



    public void inicializarCiudades(){

        if(ciudades.size()== 0){

            ciudades.add(new Ciudad(1 , "Asuncion"));
            ciudades.add(new Ciudad(2 , "Fernando de la Mora"));
            ciudades.add(new Ciudad(3 , "San Lorenzo"));
            ciudades.add(new Ciudad(4 , "Lambare"));
            ciudades.add(new Ciudad(5 , "Capiata"));
            ciudades.add(new Ciudad(6 , "Ñemby"));
          //  ciudades.add(new Ciudad(7 , "Posta Ybycua"));
            ciudades.add(new Ciudad(8 , "Itagua"));
            ciudades.add(new Ciudad(9 , "Aregua"));
          //  ciudades.add(new Ciudad(10 , "Benjamin Aceval"));
          //  ciudades.add(new Ciudad(11 , "Altos"));
            ciudades.add(new Ciudad(12 , "Villeta"));
            ciudades.add(new Ciudad(13 , "Guarambare"));
            ciudades.add(new Ciudad(14 , "San Antonio"));
            ciudades.add(new Ciudad(15 , "Ita"));
            ciudades.add(new Ciudad(22 , "Luque"));
            ciudades.add(new Ciudad(23 , "Limpio"));
            ciudades.add(new Ciudad(35 , "Mariano R. Alonso"));
        }

    }
}
