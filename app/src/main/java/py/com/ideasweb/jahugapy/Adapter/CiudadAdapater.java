package py.com.ideasweb.jahugapy.Adapter;

import android.app.Activity;
import android.content.Context;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;
import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.pojo.Ciudad;
import py.com.ideasweb.jahugapy.ui.fragments.CanchaCiudadFragment;
import py.com.ideasweb.jahugapy.ui.fragments.CercanosFragment;
import py.com.ideasweb.jahugapy.ui.fragments.CiudadesFragment;
import py.com.ideasweb.jahugapy.utilities.Utilities;

/**
 * Created by jaime on 18/11/16.
 */

public class CiudadAdapater extends ArrayAdapter<Ciudad> {
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public CiudadAdapater(Context context, List<Ciudad> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Obtener inflater.
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // ¿Existe el view actual?
        if (null == convertView) {
            convertView = inflater.inflate(
                    R.layout.list_ciudades,
                    parent,
                    false);
        }


        // Referencias UI.
        TextView name = (TextView) convertView.findViewById(R.id.tv_ciudad_name);
        TextView cantidad = (TextView) convertView.findViewById(R.id.tv_ciudad_cantidad);


        // Lead actual.
        final Ciudad ciudad = getItem(position);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Context context = getContext();
                Bundle args = new Bundle();
                args.putString("idciudad", String.valueOf(ciudad.getIddistrito()));
                args.putString("ciudad", String.valueOf(ciudad.getDescripcion()));
                Utilities.back = true;

                FragmentManager manager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                Fragment cercanos = new CanchaCiudadFragment();
                cercanos.setArguments(args);
                trans.replace(R.id.root_frame, cercanos);
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack(null);
                trans.commit();


            }
        });

        // Setup.
       // Glide.with(getContext()).load(R.drawable.organization_48).into(avatar);
        name.setText(ciudad.getDescripcion());
        cantidad.setText(String.valueOf(ciudad.getCantidad()));

        // ir al mapa




        return convertView;
    }




}
