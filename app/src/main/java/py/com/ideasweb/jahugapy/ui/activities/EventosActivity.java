package py.com.ideasweb.jahugapy.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import py.com.ideasweb.jahugapy.Adapter.CanchaAdaptador;
import py.com.ideasweb.jahugapy.Adapter.EventosAdaptador;
import py.com.ideasweb.jahugapy.R;
import py.com.ideasweb.jahugapy.api.manager.ThreadManager;
import py.com.ideasweb.jahugapy.config.Config;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.Evento;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;
import py.com.ideasweb.jahugapy.ui.fragments.CercanosFragment;
import py.com.ideasweb.jahugapy.ui.fragments.IReciclerView;
import py.com.ideasweb.jahugapy.ui.fragments.IReciclerViewEvento;
import py.com.ideasweb.jahugapy.ui.presentador.ReciclerViewPresenter;
import py.com.ideasweb.jahugapy.utilities.Share;
import py.com.ideasweb.jahugapy.utilities.Utilities;

public class EventosActivity extends AppCompatActivity implements IReciclerViewEvento {
    private Toolbar mToolbar;
    TextView mTitle;

    private RecyclerView eventos;
    SwipeRefreshLayout refreshLayout;
    private LinearLayout sindatos;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos);

        // se agrega el toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        //se centra el titulo
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        mTitle.setText(mToolbar.getTitle());
        //se oculta el title por defecto
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresIndicatorEventos);
        sindatos =(LinearLayout) findViewById(R.id.sinDatosEventos);
        eventos = (RecyclerView) findViewById(R.id.rvEventos);

        new ReciclerViewPresenter(EventosActivity.this, getApplicationContext(), false);

        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Utilities.isNetworkConnected(getApplicationContext())) {
                    new ReciclerViewPresenter(EventosActivity.this, getApplicationContext(), true);

                }else{
                    Toast.makeText(getApplicationContext(), "Verfifique su conexion y recarge", Toast.LENGTH_LONG).show();
                }
                refreshLayout.setRefreshing(false);
            }
        });

        fab =(FloatingActionButton) findViewById(R.id.fabEvento);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEvent(v);
            }
        });

    }

    //infla en el activity el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // menu de opciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mabout:
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .icon(getResources().getDrawable(R.mipmap.ic_launcher))
                        .customView(R.layout.layout_acercade, wrapInScrollView)
                        .positiveText("OK")
                        .show();
                break;
            case R.id.mshare:
                Share.shareRedeSociales(this, Config.API_DOWNLOAD_APK, "JahugaPy - Encuentra las canchas deportivas cercanas a ti!!");
                break;
            case R.id.mcontact:
                Intent intent = new Intent(this, InfoActivity.class);
                intent.putExtra("fragment", "contacto");
                startActivity(intent);
                finish();
                break;
            case android.R.id.home:
                Intent i = new Intent(EventosActivity.this, Main2Activity.class);
                startActivity(i);
                finish();

                break;
            case R.id.mfanPage:
                irFacebook();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){

        if(keyCode == KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(EventosActivity.this, Main2Activity.class);
            startActivity(intent);
            finish();

        }
        return super.onKeyDown(keyCode, event);
    }

    public void irFacebook( ) {

        try {
            getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/169405010203582"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent=  new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/jahugapy"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkJaime(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://jaimeferreira91"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/jaimeferreira91"));
            startActivity(intent);
        }
    }

    //Link redes sociales
    public void irLinkMarcos(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://marcos-cespedes-8aa92319"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/marcos-cespedes-8aa92319"));
            startActivity(intent);
        }
    }

    public void irLinkEmilio(View v) {

        try {
            v.getContext().getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://juan-emilio-espinola-874716125"));
            startActivity(intent);
        } catch (Exception e) {
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/juan-emilio-espinola-874716125"));
            startActivity(intent);
        }
    }

    public void addEvent(View view){

        Intent ie = new Intent(EventosActivity.this, AgregarEventoActivity.class);
        startActivity(ie);
        finish();


    }

    @Override
    public void generarLineaLayoutVertical() {
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        eventos.setLayoutManager(llm);
    }

    @Override
    public EventosAdaptador crearAdaptador(List<Evento> eventos) {
        EventosAdaptador adaptador = new EventosAdaptador(eventos, getParent());
        return adaptador;
    }

    @Override
    public void inicializarAdaptadorRV(EventosAdaptador adapatador) {
        eventos.setAdapter(adapatador);
        if(ReciclerViewPresenter.listeventos.size() == 0){
            sindatos.setVisibility(View.VISIBLE);
        }
    }


}
