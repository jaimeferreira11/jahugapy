package py.com.ideasweb.jahugapy.ui.presentador;

import py.com.ideasweb.jahugapy.pojo.Ciudad;
import py.com.ideasweb.jahugapy.pojo.MiUbicacion;

/**
 * Created by jaime on 05/12/16.
 */

public interface IRecyclerViewPresenter {

    public void obtenerCanchasCercanasApi(Boolean reload);
    public void mostrarCanchasCercanasRV();

    public void obtenerCanchasCiudadApi(String idciudad, Boolean reload);
    public void mostrarCanchasCiudadRV();

    public void searchCanchasApi(Boolean reload);
    public void searchCanchasCercanasRV();

    public void filtroCanchasApi(String json, Boolean reload);
    public void filtroCanchasCercanasRV();

    public void obtenerEventosApi( Boolean reload);
    public void mostartEventosRV();


}
