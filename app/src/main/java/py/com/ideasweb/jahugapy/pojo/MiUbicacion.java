package py.com.ideasweb.jahugapy.pojo;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by jaime on 03/12/16.
 */

public class MiUbicacion {
    private static  Double latitud;
    private  static  Double Longitud;
    private static boolean ban = false;

    public MiUbicacion() {
    }


    public static Double getLatitud() {
        return latitud;
    }

    public static void setLatitud(Double latitud) {
        MiUbicacion.latitud = latitud;
    }

    public static Double getLongitud() {
        return Longitud;
    }

    public static void setLongitud(Double longitud) {
        Longitud = longitud;
    }
    public static boolean getBan() {
        return ban;
    }

    public static void setBan(boolean ban) {
        MiUbicacion.ban = ban;
    }

    public static void guardarUbicacion(Context context){
        SharedPreferences SPUbicacion = context.getSharedPreferences("ubicacion", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = SPUbicacion.edit();
        editor.putString("latitud", String.valueOf(MiUbicacion.getLatitud()));
        editor.putString("longitud", String.valueOf(MiUbicacion.getLongitud()));
        editor.commit();
    }
}
