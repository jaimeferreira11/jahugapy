package py.com.ideasweb.jahugapy.ui.fragments;

import java.util.List;

import py.com.ideasweb.jahugapy.Adapter.CanchaAdaptador;
import py.com.ideasweb.jahugapy.Adapter.EventosAdaptador;
import py.com.ideasweb.jahugapy.pojo.Cancha;
import py.com.ideasweb.jahugapy.pojo.Evento;

/**
 * Created by jaime on 05/12/16.
 */

public interface IReciclerViewEvento {

    public void generarLineaLayoutVertical();
    public EventosAdaptador crearAdaptador(List<Evento> eventos);
    public void inicializarAdaptadorRV(EventosAdaptador adapatador);

}
